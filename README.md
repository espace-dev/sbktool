-------
SBK
-------

`sbk`, for **solar baking toolbox**, is a set of tools to generate indicators and maps 
for solar bakery decision aid and planning from scratch.

## Contributing

### Development and improvement

* Benjamin Pillot
* Guillaume Guimbretière
* Corrie Mathiak
* Christophe Révillion
* Romain Authier

### Conceptualization and Coordination

* Benjamin Pillot
* Guillaume Guimbretière

### Scientific projects

* SoCoMa

<br/>

![image](docs/espace-dev-ird.png)
