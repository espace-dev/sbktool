# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""

__version__ = '0.1'
__author__ = "Benjamin Pillot"
__email__ = "benjaminpillot@riseup.net"

import numpy as np

SOLAR_RADIATION_SCALE_FACTOR = 10
H5_SHADED_DNI_DS_NAME = "shaded_dni"
H5_SATELLITE_DNI_DS_NAME = "dni"
H5_HORIZON_DS_NAME = "elevation"
HORIZON_VS_SUN_DTYPE = np.int8
H5_HORIZON_VS_SUN_DS_NAME = "horizon_vs_sun"
H5_DEM_DS_NAME = "dem"
