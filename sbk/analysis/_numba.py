import numpy as np
from numba import jit

from numba.core.errors import NumbaDeprecationWarning, NumbaPendingDeprecationWarning
import warnings

warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)


@jit(nopython=True, nogil=True)
def _get_bread_accessibility(pop_density,
                             nominal_bread_production,
                             amount_of_bread_produced_per_capita,
                             prob_of_being_a_consumer,
                             prob_of_being_a_b_consumer,
                             nominal_radius_of_access):
    """

    Parameters
    ----------
    pop_density: list
    nominal_bread_production: int or float
    amount_of_bread_produced_per_capita: int or float
    prob_of_being_a_consumer: np.ndarray
    nominal_radius_of_access: int or float

    Returns
    -------

    """

    b_a = []
    for pop_d in pop_density:
        avg_radius = nominal_bread_production / (2 * np.pi * pop_d *
                                                 prob_of_being_a_consumer *
                                                 prob_of_being_a_b_consumer *
                                                 amount_of_bread_produced_per_capita)
        result = avg_radius <= nominal_radius_of_access
        b_a.append(result.sum() / result.size)

    return b_a


@jit(nopython=True, nogil=True)
def _get_bread_viability(items,
                         nominal_bread_production,
                         bp_noise):
    """

    Parameters
    ----------
    items: list
        items containing bread accessibility
        and corresponding bread production
    nominal_bread_production: np.ndarray

    Returns
    -------

    """
    b_v = []
    for b_a, b_p in zip(*items):
        prob_prod_is_nominal = (b_p * bp_noise) >= nominal_bread_production
        b_v.append(b_a * prob_prod_is_nominal.sum() / prob_prod_is_nominal.size)

    return b_v


@jit(nopython=True, nogil=True)
def _get_probability_of_consumption(items,
                                    bread_consumption,
                                    probability_of_being_a_consumer,
                                    bp_noise):
    """ Compute probability of consumption

    Parameters
    ----------
    items: list
        items containing bread prod and
        corresponding nb of inhabitants
    bread_consumption: np.ndarray
        probability distribution
    probability_of_being_a_consumer: np.ndarray
        probability distribution
    bp_noise: np.ndarray

    Returns
    -------

    """
    p_c = []
    for bp, ni in zip(*items):
        result = bread_consumption >= (bp * bp_noise) / (ni * probability_of_being_a_consumer)
        p_c.append(result.sum() / result.size)

    return p_c


def _get_probability_of_b_consumption(items,
                                      bread_consumption,
                                      cdf_d,
                                      expectation_0_d,
                                      p_c_b,
                                      p_h_c,
                                      bp_noise):
    """ Compute transitional version of probability consumption

    Parameters
    ----------
    items: list
        items containing bread prod and
        corresponding pop density
    bread_consumption: np.ndarray
        probability distribution of
        the bread consumption qty
    cdf_d: float
    expectation_0_d: float
    p_c_b: np.ndarray
        probability distribution of
        the probability of b-consumption
    p_h_c
    bp_noise

    Returns
    -------

    """
    p_c = []
    for bp, eta_h in zip(*items):
        n_c_b = 2 * np.pi * p_c_b * p_h_c * eta_h * cdf_d * expectation_0_d
        result = bread_consumption >= bp * bp_noise / n_c_b
        p_c.append(result.sum() / result.size)

    return p_c


@jit(nopython=True, nogil=True)
def _get_bread_turnover(items,
                        bread_consumption,
                        cdf_d,
                        expectation_0_d,
                        p_c_b,
                        p_h_c,
                        bp_noise,
                        vector_size):

    bread_turnover = []

    for bp, eta_h in zip(*items):

        r_b = []
        bread_prod = np.linspace(0, bp, vector_size)
        n_c_b = 2 * np.pi * p_c_b * p_h_c * eta_h * cdf_d * expectation_0_d

        for b_p in bread_prod:
            result = bread_consumption >= b_p * bp_noise / n_c_b
            r_b.append(b_p * result.sum() / result.size)

        bread_turnover.append(max(r_b))

    return bread_turnover
