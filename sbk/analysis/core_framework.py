import numpy as np
import pandas as pd

from functools import partial

from pyrasta.raster import Raster


def _get_values_from(variable, no_data):
    """ Get values based on variable type
    """
    if isinstance(variable, Raster):
        out = variable.read_array()
    elif isinstance(variable, (pd.Series, pd.DataFrame)):
        out = variable.values
    elif isinstance(variable, (list, tuple, np.ndarray)):
        out = np.asarray(variable)
    else:
        return None

    if no_data:
        out[out == no_data] = np.nan

    return out


def _get_format_from(variable):
    """ Return function based on format of input variable
    """
    def raster_format():
        return partial(Raster.from_array,
                       crs=variable.crs,
                       bounds=variable.bounds,
                       no_data=variable.no_data)

    def series_format():
        return partial(pd.Series,
                       index=variable.index,
                       dtype=variable.dtype)

    def dataframe_format():
        return partial(pd.DataFrame,
                       index=variable.index,
                       columns=variable.columns,
                       dtype=variable.values.dtype)

    def array_format():
        return np.asarray

    if isinstance(variable, Raster):
        return raster_format()
    elif isinstance(variable, pd.Series):
        return series_format()
    elif isinstance(variable, pd.DataFrame):
        return dataframe_format()
    else:
        return array_format()


class Variable:

    def __init__(self, variable, no_data=None):
        """ Class constructor

        Description
        -----------
        Build Variable instance from input argument

        Parameters
        ----------
        variable:
            Accepted type of variable :
                * pyrasta.raster.Raster
                * pandas.Series
                * numpy array
                * list, tuple
        no_data: int or float
            No data value in variable
        """
        self._values = _get_values_from(variable, no_data)
        self._no_data = no_data
        self._shape = self._values.shape
        self._to_out_format = _get_format_from(variable)

    def __iter__(self):
        return self._values.flat

    def __getitem__(self, item):
        return self._values.flat[item]

    def __mul__(self, other):
        try:
            return self.build_from(self.values * other.values, self)
        except AttributeError:
            return self.build_from(self.values * other, self)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __setitem__(self, key, value):
        self._values.flat[key] = value

    def to_out_format(self):
        """ Convert values to Variable original format

        Returns
        -------

        """
        return self._to_out_format(self._values)

    @classmethod
    def build_from(cls, values, other):
        """ Build new Variable from values based
        on other underlying format

        Parameters
        ----------
        values: np.ndarray
        other: Variable

        Returns
        -------

        """
        if values.shape != other.values.shape:
            try:
                values = values.reshape(other.values.shape)
            except ValueError:
                raise ValueError("Number of elements must be the same !")

        if other.no_data:
            values[np.isnan(values)] = other.no_data
        return cls(other._to_out_format(values),
                   no_data=other.no_data)

    @classmethod
    def full(cls, value, other):
        """ Build new variable from scalar and
        "other" underlying format

        Parameters
        ----------
        value: int or float
        other: Variable

        Returns
        -------

        """
        return cls(other._to_out_format(np.full(other._shape, value)))

    @classmethod
    def zeros(cls, other):
        """ Build new empty Variable from
        other underlying format

        Parameters
        ----------
        other: Variable

        Returns
        -------

        """
        return cls(other._to_out_format(np.zeros(other._shape)))

    @property
    def no_data(self):
        return self._no_data

    @property
    def size(self):
        return self.values.size

    @property
    def values(self):
        return self._values
