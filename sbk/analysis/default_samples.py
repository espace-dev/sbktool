import numpy as np

from scipy.stats import truncnorm, foldnorm


def _truncnorm(mean, std, _range):
    return truncnorm(a=(_range[0] - mean) / std,
                     b=(_range[1] - mean) / std,
                     loc=mean,
                     scale=std)


# Nominal bread production (in kg)
YEARLY_VERY_LOW_X_P = _truncnorm(8000, 2000, [5000, 11000])
YEARLY_LOW_X_P = _truncnorm(12000, 2000, [8000, 16000])
YEARLY_MEDIUM_X_P = _truncnorm(15000, 2500, [11000, 19000])
YEARLY_HIGH_X_P = _truncnorm(18000, 3000, [14000, 22000])
YEARLY_VERY_HIGH_X_P = _truncnorm(22800, 3000, [18000, 27000])

# Yearly bread consumption (in kg)
YEARLY_LOW_Q_C = _truncnorm(35, 25, [0.5, np.inf])
YEARLY_MEDIUM_Q_C = _truncnorm(50, 25, [0.5, np.inf])
YEARLY_HIGH_Q_C = _truncnorm(65, 25, [0.5, np.inf])

# Monthly bread consumption (in kg)
MONTHLY_LOW_Q_C = _truncnorm(3.5, 2.5, [0.25, np.inf])
MONTHLY_MEDIUM_Q_C = _truncnorm(5.0, 2.5, [0.25, np.inf])
MONTHLY_HIGH_Q_C = _truncnorm(6.5, 2.5, [0.25, np.inf])

# Probability for a human to be a bread consumer (P_h,c)
HIGH_P_H_C = _truncnorm(.95, .05, [0, 1])
MEDIUM_P_H_C = _truncnorm(.75, .1, [0, 1])
LOW_P_H_C = _truncnorm(.5, .1, [0, 1])

# Default probability of b-consumption
HIGH_P_C_B = _truncnorm(.95, .05, [0, 1])
MEDIUM_P_C_B = _truncnorm(.75, .05, [0, 1])
LOW_P_C_B = _truncnorm(.5, .1, [0, 1])

# Default R-distribution of the consumers around bakery (in km)
LOW_R_DIST = foldnorm(c=.9, loc=0, scale=.1)
MEDIUM_R_DIST = foldnorm(c=.9, loc=0, scale=.25)
HIGH_R_DIST = foldnorm(c=.9, loc=0, scale=.4)
