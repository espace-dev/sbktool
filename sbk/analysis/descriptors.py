from abc import abstractmethod

import xarray as xr

from sbk.bakery import Bakery
from sbk.grid_db.base import RegularGridDB
from sbk.utils import this_is


class Descriptor:
    """ Mapper class definition
        Provide raster mapping
    """
    name = None  # str
    no_data_value = None
    dtype = None  # str
    scale_factor = None  # int
    vectorized = None  # bool

    _data = None

    def __init__(self, bakery=None):
        """ Constructor

        Parameters
        ----------
        bakery: sbk.bakery.Bakery
            Bakery for which Descriptor must be computed
        """
        if bakery is not None:
            self.bakery = bakery

    def compute(self, grid_db, **kwargs):
        """ Compute descriptor

        Returns
        -------
        Descriptor

        """
        self._data = self.apply_func(grid_db,
                                     self.fhandle,
                                     self.name,
                                     self.bakery.organization_model.schedule(grid_db.time),
                                     **kwargs)

        return self

    def resample(self, rule, resampler):
        """ Resample descriptor

        Parameters
        ----------
        rule: str
            Resampling rule for descriptor as a string
            {'D', 'M', 'Y', etc.}
        resampler: function or str
            Valid resampler string or function ("mean", "sum", etc.)

        Returns
        -------

        """
        self._data = self._data.resample(resampling_rule=rule,
                                         resampler=resampler)

        return self

    def resample_with_strategy(self, resampler="sum"):
        """ Resampling based on bakery strategy

        Returns
        -------

        """
        self._data = (
            self._data.resample(
                resampling_rule=self.bakery.organization_model.rule,
                resampler=self.bakery.organization_model.strategy(resampler)))

        return self

    def save(self, filename):
        """ Save GridDB data to netcdf

        Parameters
        ----------
        filename: str
            File path to write to

        Returns
        -------

        """
        try:
            self.data.save(filename,
                           scale_factor=self.scale_factor,
                           dtype=self.dtype,
                           no_data_value=self.no_data_value)
        except AttributeError:
            raise ValueError("Descriptor has not been computed yet")

    @property
    def apply_func(self):
        if self.vectorized:
            return _apply_func
        else:
            return _apply_func_unvectorized

    @property
    def data(self):
        return self._data

    @property
    @abstractmethod
    def fhandle(self):
        pass

    @property
    def bakery(self):
        return self._bakery

    @bakery.setter
    @this_is(Bakery)
    def bakery(self, value):
        self._bakery = value


class Batches(Descriptor):

    name = "NbBatches"
    no_data_value = -1
    dtype = "int8"
    scale_factor = 1
    vectorized = False

    def compute(self, dni, **kwargs):
        """ Compute nb of batches for given DNI

        Parameters
        ----------
        dni: sbk.grid_db.base.RegularGridDB or sbk.grid_db.base.SparseGridDB
            solar DNI

        Returns
        -------

        """
        return super().compute(dni,
                               time_step=dni.timedelta / 3600)

    @property
    def fhandle(self):
        try:
            return self.bakery.oven.batches
        except AttributeError:
            raise ValueError("Bakery is not defined")


class Bakable(Descriptor):

    name = "Bakable"
    no_data_value = -1
    dtype = "int8"
    scale_factor = 1
    vectorized = True

    def compute(self, batches, threshold=1, **kwargs):
        """ Compute bakable day from given nb of batches

        Parameters
        ----------
        batches: sbk.grid_db.base.RegularGridDB or sbk.grid_db.base.SparseGridDB
            Nb of batches
        threshold: int
            Nb of batches for which period is considered "bakeable"

        Returns
        -------

        """
        return super().compute(batches,
                               threshold=threshold)

    @property
    def fhandle(self):
        return is_bakable


def _apply_func(grid_db, fhandle, name, schedule, **kwargs):

    new_data = fhandle(schedule(grid_db), **kwargs)
    new_data.name = name

    return grid_db.build_from_data(new_data)


def _apply_func_unvectorized(grid_db, fhandle, name, schedule, **kwargs):
    """ Apply function fhandle on every points
    of the grid (unvectorized), over its entire
    time series
    """
    data = schedule(grid_db.data)

    if isinstance(grid_db, RegularGridDB):  # apply function for regular GridDB
        new_data = xr.apply_ufunc(fhandle,
                                  data,
                                  kwargs=kwargs,
                                  input_core_dims=[[grid_db._dims["t"]]],
                                  output_core_dims=[[grid_db._dims["t"]]],
                                  dask="parallelized",
                                  # dask="parallelized",
                                  vectorize=True,
                                  dask_gufunc_kwargs={"allow_rechunk": True})

    else:  # apply function for sparse GridDB
        pass

    new_data.name = name

    return grid_db.build_from_data(new_data)


def is_bakable(batches, threshold):
    """

    Parameters
    ----------
    batches: sbk.grid_db.base.RegularGridDB or sbk.grid_db.base.SparseGridDB
    threshold: int

    Returns
    -------

    """
    if batches.name != Batches.name:
        raise ValueError(f"Input GridDB name should be '{Batches.name}' "
                         f"but is '{batches.name}'")

    return (batches.data > threshold).astype("int")
