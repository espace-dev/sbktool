import multiprocessing as mp
import numpy as np

from abc import abstractmethod
from functools import partial

from scipy.stats._distn_infrastructure import rv_frozen
from tqdm import tqdm

from sbk.analysis import Variable
from sbk.analysis._numba import _get_probability_of_consumption, _get_bread_accessibility, \
    _get_bread_viability, _get_probability_of_b_consumption, _get_bread_turnover
from sbk.analysis.default_samples import YEARLY_MEDIUM_Q_C, MEDIUM_P_H_C, MEDIUM_P_C_B, \
    MEDIUM_R_DIST, YEARLY_MEDIUM_X_P
from sbk.utils import split_into_chunks, typed_property, force_type

MP_CHUNK_SIZE = 10


distribution = partial(typed_property, expected_type=rv_frozen)
variable = partial(typed_property, expected_type=Variable)


def _bread_accessibility(pop_density,
                         bread_consumption,
                         prob_of_being_consumer,
                         prob_of_being_b_consumer,
                         nominal_bread_production,
                         nominal_radius_of_access,
                         prob_bread_consumption,
                         distribution_size,
                         random_state,
                         chunksize,
                         nb_processes):

    generator = (list(pop_d) for pop_d in
                 split_into_chunks(pop_density.ravel(), chunksize))
    q_pc_x = bread_consumption.ppf(1 - prob_bread_consumption)
    p_c_h = prob_of_being_consumer.rvs(size=distribution_size,
                                       random_state=random_state)
    p_c_b = prob_of_being_b_consumer.rvs(size=distribution_size,
                                         random_state=random_state)

    try:
        bread_production = nominal_bread_production.rvs(size=distribution_size,
                                                        random_state=random_state)
    except AttributeError:
        bread_production = nominal_bread_production

    try:
        mu_r_nom = nominal_radius_of_access.rvs(size=distribution_size,
                                                random_state=random_state)
    except AttributeError:
        mu_r_nom = nominal_radius_of_access

    with mp.Pool(processes=nb_processes) as pool:
        bar_length = pop_density.size // chunksize + min(1, pop_density.size % chunksize)
        b_a = list(tqdm(pool.imap(partial(_get_bread_accessibility,
                                          nominal_bread_production=bread_production,
                                          amount_of_bread_produced_per_capita=q_pc_x,
                                          prob_of_being_a_consumer=p_c_h,
                                          prob_of_being_a_b_consumer=p_c_b,
                                          nominal_radius_of_access=mu_r_nom),
                                  generator,
                                  chunksize=MP_CHUNK_SIZE),
                        total=bar_length,
                        desc="Compute bread accessibility"))

    return np.asarray([value for array in b_a for value in array])


def _bread_viability(bread_accessibility,
                     bread_production,
                     nominal_bread_production,
                     bread_prod_uncertainty,
                     distribution_size,
                     random_state,
                     chunksize,
                     nb_processes):

    generator = ((list(b_a), list(b_p)) for b_a, b_p
                 in zip(split_into_chunks(bread_accessibility.ravel(), chunksize),
                        split_into_chunks(bread_production.ravel(), chunksize)))

    x_p_star = nominal_bread_production.rvs(size=distribution_size,
                                            random_state=random_state)

    try:
        bp_noise = bread_prod_uncertainty.rvs(size=distribution_size,
                                              random_state=random_state)
    except AttributeError:
        bp_noise = 1

    with mp.Pool(processes=nb_processes) as pool:
        bar_length = bread_production.size // chunksize + \
                     min(1, bread_production.size % chunksize)
        b_v = list(tqdm(pool.imap(partial(_get_bread_viability,
                                          nominal_bread_production=x_p_star,
                                          bp_noise=bp_noise),
                                  generator,
                                  chunksize=MP_CHUNK_SIZE),
                        total=bar_length,
                        desc="Compute bread viability"))

    return np.asarray([value for array in b_v for value in array])


def _probability_of_consumption(bread_production,
                                pop_density,
                                bread_consumption,
                                prob_of_being_consumer,
                                bread_prod_uncertainty,
                                distance,
                                distribution_size,
                                random_state,
                                chunksize,
                                nb_processes):

    if distance:
        # If distance, pop_density is pop density,
        # so convert into a count (nb of inhabitants)
        pop_density = pop_density * np.pi * distance ** 2

    generator = ((list(b_p), list(pop_d)) for b_p, pop_d
                 in zip(split_into_chunks(bread_production.ravel(), chunksize),
                        split_into_chunks(pop_density.ravel(), chunksize)))

    b_c = bread_consumption.rvs(size=distribution_size,
                                random_state=random_state)
    p_h_c = prob_of_being_consumer.rvs(size=distribution_size,
                                       random_state=random_state)
    if bread_prod_uncertainty is None:
        bp_noise = 1
    else:
        bp_noise = bread_prod_uncertainty.rvs(size=distribution_size,
                                              random_state=random_state)
    with mp.Pool(processes=nb_processes) as pool:
        bar_length = bread_production.size // chunksize + \
                     min(1, bread_production.size % chunksize)
        p_c = list(tqdm(pool.imap(partial(_get_probability_of_consumption,
                                          bread_consumption=b_c,
                                          probability_of_being_a_consumer=p_h_c,
                                          bp_noise=bp_noise),
                                  generator,
                                  chunksize=MP_CHUNK_SIZE),
                   total=bar_length,
                   desc="Compute probability of consumption"))

    return np.asarray([value for array in p_c for value in array])


def _probability_of_b_consumption(bread_production,
                                  pop_density,
                                  bread_consumption,
                                  prob_of_b_consumption,
                                  prob_of_being_a_consumer,
                                  r_distribution,
                                  bread_prod_uncertainty,
                                  distance,
                                  distribution_size,
                                  random_state,
                                  chunksize,
                                  nb_processes,
                                  function,
                                  description):

    # pop_density = pop_density * np.pi * distance**2

    generator = ((list(b_p), list(pop_d)) for b_p, pop_d
                 in zip(split_into_chunks(bread_production.ravel(), chunksize),
                        split_into_chunks(pop_density.ravel(), chunksize)))

    b_c = bread_consumption.rvs(size=distribution_size,
                                random_state=random_state)
    p_h_c = prob_of_being_a_consumer.rvs(size=distribution_size,
                                         random_state=random_state)
    p_c_b = prob_of_b_consumption.rvs(size=distribution_size,
                                      random_state=random_state)

    expectation_0_d = r_distribution.expect(lb=0, ub=distance)
    cdf_d = r_distribution.cdf(distance)

    if bread_prod_uncertainty is None:
        bp_noise = 1
    else:
        bp_noise = bread_prod_uncertainty.rvs(size=distribution_size,
                                              random_state=random_state)

    with mp.Pool(processes=nb_processes) as pool:
        bar_length = bread_production.size // chunksize + \
                     min(1, bread_production.size % chunksize)
        p_c = list(tqdm(pool.imap(partial(function,
                                          bread_consumption=b_c,
                                          cdf_d=cdf_d,
                                          expectation_0_d=expectation_0_d,
                                          p_c_b=p_c_b,
                                          p_h_c=p_h_c,
                                          bp_noise=bp_noise,
                                          vector_size=2000),
                                  generator,
                                  chunksize=MP_CHUNK_SIZE),
                   total=bar_length,
                   desc=description))

    return np.asarray([value for array in p_c for value in array])


class Indicator:
    """ Indicator can be a time series,
        a raster or a geo grid

    """
    _variable = None

    @abstractmethod
    def compute(self, *args, **kwargs):
        pass

    def to_out_format(self):
        """ Retrieve indicator under the
        format given by output Variable
        (Raster, Series, numpy array)
        """
        return self._variable.to_out_format()

    @property
    def variable(self):
        return self._variable


@force_type(bread_production=variable,
            pop_density=variable,
            bread_consumption=distribution,
            prob_of_being_a_consumer=distribution)
class ProbabilityOfConsumption(Indicator):
    """ Probability of consumption definition
        Based on the socio-technical framework
        developed for sustainable baking
    """

    def __init__(self,
                 bread_production,
                 pop_density,
                 bread_consumption=YEARLY_MEDIUM_Q_C,
                 prob_of_being_a_consumer=MEDIUM_P_H_C,
                 bread_production_uncertainty=None):
        """ Build class instance

        Description
        -----------
        Probability of consumption as defined in
        the analytical framework

        Parameters
        ----------
        bread_production: sbk.analysis.Variable
            Bread production (in kg)
        pop_density: sbk.analysis.Variable
            Population density (nb of inhabitants per km²)
        bread_consumption: scipy.stats.distributions.rv_frozen
            Bread consumption (in kg/capita)
            as a probability distribution
        prob_of_being_a_consumer: scipy.stats.distributions.rv_frozen
            Probability for a human to be
            a bread consumer
        bread_production_uncertainty: scipy.stats.distributions.rv_frozen
            Uncertainty on bread production as a
            probability distribution
        """
        self.bread_production = bread_production
        self.bread_consumption = bread_consumption
        self.pop_density = pop_density
        self.prob_of_being_a_consumer = prob_of_being_a_consumer
        self.bread_production_uncertainty = bread_production_uncertainty

        if self.bread_production.size != self.pop_density.size:
            raise ValueError(f"Size of Variables must be the same but are "
                             f"{self.bread_production.size} and "
                             f"{self.pop_density.size}")

    def compute(self,
                distance,
                distribution_size=10000,
                random_state=None,
                chunksize=1000,
                nb_processes=mp.cpu_count()//2,
                **kwargs):
        """ Compute probability of consumption

        Parameters
        ----------
        distance: int or float
            radius distance used in computation
            for population count
            if None, population is regarded as
            population count
        distribution_size: int
            Size of probability distributions
            used in computation
        random_state: int
            Used to produce reproducible results
        chunksize
        nb_processes

        Returns
        -------

        """
        p_c = _probability_of_consumption(self.bread_production.values,
                                          self.pop_density.values,
                                          self.bread_consumption,
                                          self.prob_of_being_a_consumer,
                                          self.bread_production_uncertainty,
                                          distance,
                                          distribution_size,
                                          random_state,
                                          chunksize,
                                          nb_processes)

        self._variable = Variable.build_from(p_c,
                                             self.bread_production)

        return self


@force_type(prob_of_b_consumption=distribution,
            r_distribution=distribution)
class ProbabilityOfBConsumption(ProbabilityOfConsumption):

    def __init__(self,
                 bread_production,
                 pop_density,
                 prob_of_b_consumption=MEDIUM_P_C_B,
                 r_distribution=MEDIUM_R_DIST,
                 bread_consumption=YEARLY_MEDIUM_Q_C,
                 prob_of_being_a_consumer=MEDIUM_P_H_C,
                 bread_production_uncertainty=None):
        """

        Description
        -----------
        Transitional version of the probability
        of consumption as defined in the analytical framework

        Parameters
        ----------
        bread_production: sbk.analysis.Variable
            Bread production (in kg)
        pop_density: sbk.analysis.Variable
            Nb of inhabitants (as pop density per km²)
        prob_of_b_consumption: scipy.stats.distributions.rv_frozen
        r_distribution: scipy.stats.distributions.rv_frozen
        bread_consumption: scipy.stats.distributions.rv_frozen
            Bread consumption (in kg/capita)
            as a probability distribution
        prob_of_being_a_consumer: scipy.stats.distributions.rv_frozen
            Probability for a human to be
            a bread consumer
        bread_production_uncertainty: scipy.stats.distributions.rv_frozen
            Uncertainty on bread production as a
            probability distribution
        """
        super().__init__(bread_production,
                         pop_density,
                         bread_consumption,
                         prob_of_being_a_consumer,
                         bread_production_uncertainty)

        self.prob_of_b_consumption = prob_of_b_consumption
        self.r_distribution = r_distribution

    def compute(self,
                distance,
                distribution_size=10000,
                random_state=None,
                chunksize=1000,
                nb_processes=mp.cpu_count()//2,
                **kwargs):

        try:
            function = kwargs["function"]
            description = kwargs["description"]
        except KeyError:
            function = _get_probability_of_b_consumption
            description = "Compute probability of b-consumption"

        p_c = _probability_of_b_consumption(self.bread_production.values,
                                            self.pop_density.values,
                                            self.bread_consumption,
                                            self.prob_of_b_consumption,
                                            self.prob_of_being_a_consumer,
                                            self.r_distribution,
                                            self.bread_production_uncertainty,
                                            distance,
                                            distribution_size,
                                            random_state,
                                            chunksize,
                                            nb_processes,
                                            function,
                                            description)

        self._variable = Variable.build_from(p_c,
                                             self.bread_production)

        return self


class BreadTurnover(ProbabilityOfBConsumption):
    """ Bread turnover

    """

    def compute(self,
                distance,
                distribution_size=10000,
                random_state=None,
                chunksize=1000,
                nb_processes=mp.cpu_count()//2,
                **kwargs):

        return super().compute(distance,
                               distribution_size,
                               random_state,
                               chunksize,
                               nb_processes,
                               function=_get_bread_turnover,
                               description="Compute bread turnover")


class BreadAccessibility(Indicator):

    def __init__(self,
                 population_density,
                 nominal_bread_production=YEARLY_MEDIUM_X_P,
                 bread_consumption=YEARLY_MEDIUM_Q_C,
                 prob_of_being_a_consumer=MEDIUM_P_H_C,
                 prob_of_being_a_b_consumer=MEDIUM_P_C_B,
                 avg_radius_of_access=MEDIUM_R_DIST.mean(),
                 p_c_threshold=.95,
                 ):
        """ Init bread accessibility indicator

        Parameters
        ----------
        population_density: Variable
            Population density (hab/km²)
        bread_consumption: rv_frozen
            Bread consumption probability
            distribution (in kg/capita)
        nominal_bread_production: int or float or rv_frozen
            Nominal bread production (in kg)
        prob_of_being_a_consumer: rv_frozen
            Probability for a human to be
            a bread consumer
        avg_radius_of_access: int or float or rv_frozen
            Nominal distance range corresponding to
            nominal production fully delivered (in km)
        p_c_threshold: float
            Fixed probability value X
            close to 1 (.95, .99), corresponding
            to most of the bread production being
            delivered (P_c = X)
        """
        self.nb_of_inhabitants = population_density
        self.nominal_bread_production = nominal_bread_production
        self.bread_consumption = bread_consumption
        self.prob_of_being_a_consumer = prob_of_being_a_consumer
        self.prob_of_being_a_b_consumer = prob_of_being_a_b_consumer
        self.avg_radius_of_access = avg_radius_of_access
        self.p_c_threshold = p_c_threshold

    def compute(self,
                distribution_size=10000,
                random_state=None,
                chunksize=1000,
                nb_processes=mp.cpu_count() // 2,
                **kwargs):
        """

        Parameters
        ----------
        distribution_size: int
            Size of the probability distributions
            used in computation
        random_state: int
        chunksize: int
        nb_processes: int
        kwargs

        Returns
        -------

        """
        b_a = _bread_accessibility(self.nb_of_inhabitants.values,
                                   self.bread_consumption,
                                   self.prob_of_being_a_consumer,
                                   self.prob_of_being_a_b_consumer,
                                   self.nominal_bread_production,
                                   self.avg_radius_of_access,
                                   self.p_c_threshold,
                                   distribution_size,
                                   random_state,
                                   chunksize,
                                   nb_processes)

        self._variable = Variable.build_from(b_a,
                                             self.nb_of_inhabitants)

        return self


class BreadViability(Indicator):

    def __init__(self,
                 bread_accessibility,
                 bread_production,
                 nominal_bread_production=YEARLY_MEDIUM_X_P,
                 bread_production_uncertainty=None):
        """

        Parameters
        ----------
        bread_accessibility: Variable
        bread_production: Variable
        nominal_bread_production: int or float or rv_frozen
        bread_production_uncertainty: rv_frozen
            Bread production uncertainty as a
            probability distribution
        """
        self.bread_accessibility = bread_accessibility
        self.bread_production = bread_production
        self.nominal_bread_production = nominal_bread_production
        self.bread_production_uncertainty = bread_production_uncertainty

    def compute(self,
                distribution_size=10000,
                random_state=None,
                chunksize=1000,
                nb_processes=mp.cpu_count() // 2,
                **kwargs):

        b_v = _bread_viability(self.bread_accessibility.values,
                               self.bread_production.values,
                               self.nominal_bread_production,
                               self.bread_production_uncertainty,
                               distribution_size,
                               random_state,
                               chunksize,
                               nb_processes)

        self._variable = Variable.build_from(b_v,
                                             self.bread_production)

        return self
