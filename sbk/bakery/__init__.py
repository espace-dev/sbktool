# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import bottleneck as bn
import pandas as pd

from sbk.utils import this_is
from sbk.bakery.oven import *
from sbk.bakery.models import *


class OrganizationModel:
    """ Organization model class

    Returns both a schedule and a strategy
    - Schedule defines moments for baking bread
    e.g. in a week scenario, it gives both the days and
    hour ranges where it is possible to bake bread
    - Strategy defines thresholds
    e.g. the baker might bake bread only during one/two of
    the best 3 days, and only up to a maximum amount of batches
    To apply a scenario:
    1. Apply the schedule to the raw data (e.g. hourly)
        * scheduled_data = OrganizationModel.schedule(*args, **kwargs)(data)
    2. Resample data to the upper timescale (e.g. daily)
    3. Apply the strategy when resampling data to the upper timescale (e.g. weekly)
        * data.resample(rule="W").apply(OrganizationModel.strategy(*args, **kwargs))
    """
    _rule = None  # Rule-based future resampling

    @classmethod
    @abstractmethod
    def default_model(cls):
        pass

    @abstractmethod
    def schedule(self, *args, **kwargs):
        pass

    @abstractmethod
    def strategy(self, *args, **kwargs):
        pass

    @property
    def rule(self):
        return self._rule


class WeekOrganizationModel(OrganizationModel):
    """ Weekly organization model class

    """
    _rule = "W"

    day_num = dict(monday=0,
                   tuesday=1,
                   wednesday=2,
                   thursday=3,
                   friday=4,
                   saturday=5,
                   sunday=6)

    default_working_days = dict(monday=((0, 24),),
                                tuesday=((0, 24),),
                                wednesday=((0, 24),),
                                thursday=((0, 24),),
                                friday=((0, 24),),
                                saturday=((0, 24),),
                                sunday=((0, 24),))

    def __init__(self, working_days, nb_days=None, max_output=None, *args, **kwargs):
        """ Build instance

        Parameters
        ----------
        working_days: dict
            dictionary as {'monday': [(8, 12), (14, 18)],
                           'tuesday': [(9, 13), (15, 18)], etc.}
            keys are days, while items are lists with
            the different hour ranges as tuples
            (e.g. from 8 to 12 : (8, 12))
        nb_days: int
            Nb of days in the week the baker bakes bread
            according to the schedule. If the schedule considers
            4 baking days, but the strategy is 2 days, it means
            that the strategy is to get the 2 best days, i.e.
            days for which production is maximum
            default: None
        max_output: int or float
            Maximum output for each day ; e.g.: if the strategy
            is about batches, this corresponds to max nb of batches per day
            default: None

        Returns
        -------
        function

        """
        self._working_days = working_days

        if nb_days is None:
            self.nb_days = len(self._working_days.keys())

        if max_output is None:
            self.max_output = 24

    @classmethod
    def default_model(cls):
        return WeekOrganizationModel(cls.default_working_days)

    def schedule(self, time, *args, **kwargs):
        """ Build week schedule

        Parameters
        ----------
        time: xarray.DataArray or pandas.DatetimeIndex or numpy.ndarray
            datetime vector corresponding to data
        args
        kwargs

        Returns
        -------
        function

        """

        if not isinstance(time, pd.DatetimeIndex):
            time = pd.DatetimeIndex(time)

        def fhandle(_data):

            day_slot = np.zeros(time.shape, dtype=bool)

            for day, hours in self._working_days.items():
                for hour in hours:
                    day_slot = day_slot | ((time.weekday == self.day_num[day])
                                           & (time.hour >= hour[0])
                                           & (time.hour <= hour[1]))

            if _data.ndim > 1:
                day_slot = np.expand_dims(day_slot, tuple(n for n in range(1, _data.ndim)))

            return _data * day_slot

        return fhandle

    def strategy(self, resampler):
        """ Define baking strategy

        Parameters
        ----------
        resampler: str
            resampling function among {'sum', 'mean'} (default = 'sum')

        Returns
        -------
        function

        """

        def fhandle(values):

            values = np.minimum(values, self.max_output)
            if resampler == "sum":
                return -bn.partition(-values, self.nb_days)[:self.nb_days].sum()
            elif resampler == "mean":
                return -bn.partition(-values, self.nb_days)[:self.nb_days].mean()

        return fhandle

    @property
    def max_output(self):
        return self._max_output

    @max_output.setter
    @this_is((int, float))
    def max_output(self, value):
        if value < 0:
            raise ValueError("max_output must be >= 0")

        self._max_output = value

    @property
    def nb_days(self):
        return self._nb_days

    @nb_days.setter
    @this_is(int)
    def nb_days(self, value):
        if value < 1:
            raise ValueError("nb_days must be >= 1")

        self._nb_days = min(value, len(self._working_days.keys()))


class Bakery:

    def __init__(self, oven, organization_model):
        """

        Parameters
        ----------
        oven: sbk.bakery.oven.Oven
            Oven used in bakery
        organization_model: OrganizationModel
            Organization model for the bakery
        """
        self.oven = oven
        self.organization_model = organization_model

    @property
    def oven(self):
        return self._oven

    @oven.setter
    @this_is(Oven)
    def oven(self, value):
        self._oven = value

    @property
    def organization_model(self):
        return self._organization_model

    @organization_model.setter
    @this_is(OrganizationModel)
    def organization_model(self, value):
        self._organization_model = value

# schedule = ((time.weekday==1) & (time.hour >= 7) & (time.hour <= 13)) | ((time.weekday==3) & (
# time.hour >= 13) & (time.hour <= 18))
# m=time_series.groupby(by=time_series.index.dayofyear * schedule).sum()
# m = m.drop(index=0)
# index = np.unique(time_series.index[schedule].floor(freq="D"))
# m.index = index

# To retrieve the 2 maximum per week (for instance):
# import bottleneck as bn
# -bn.partition(-array, 2)[:2]
# To get the sum --> -bn.partition(-array, 2)[:2].sum()
