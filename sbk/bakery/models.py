import numpy as np

from abc import abstractmethod


def diff(array):
    return np.hstack((0, array[1:] - array[:-1]))


class OvenModel:
    """ Class used to implement oven behavior model

    """

    def __init__(self):
        pass

    @abstractmethod
    def number_of_batches(self, *args, **kwargs):
        pass


class SolarThresholdModel(OvenModel):

    def __init__(self, dni_threshold, preheat_time, baking_time):
        """

        Parameters
        ----------
        dni_threshold: float or int (in W or Wh/m²)
        preheat_time: float or int
            constant preheat time (in hours)
        baking_time: float or int
            constant baking time (in hours)
        """
        super().__init__()
        self.dni_threshold = dni_threshold
        self.preheat_time = preheat_time
        self.baking_time = baking_time

    def number_of_batches(self, dni, time_step=1):
        """ Retrieve the nb of bread batches produced by the oven

        Parameters
        ----------
        dni: numpy.array
            full time series of DNI values
            time series MUST start and end by
            zeros (corresponding to nighttime)
            be aware that first and last values
            are replaced by "no operating hours"
            (i.e. zeros) under the hood
        time_step: float
            DNI time step (in hours)

        Returns
        -------

        """
        # Nb of continuous operating hours
        # hourly_dni = dni.resample('H').mean()
        return _number_of_batches(dni,
                                  self.dni_threshold,
                                  self.preheat_time/time_step,
                                  self.baking_time/time_step)

    def total_number_of_batches(self, dni):
        """ Return total number of batches corresponding
        to the time series of dni values

        Parameters
        ----------
        dni: numpy.array
            full time series of DNI values
            time series MUST start and end by
            zeros (corresponding to night time)
            Be aware that first and last values
            are replaced by "no operating hours"
            (i.e. zeros) under the hood


        Returns
        -------

        """
        return _total_number_of_batches(dni,
                                        self.dni_threshold,
                                        self.preheat_time,
                                        self.baking_time)


def _number_of_batches(dni, dni_threshold, preheat_time, baking_time):
    """Return nb of batches time series
    """

    operating_hours = (dni >= dni_threshold).astype(np.int16)
    operating_hours[0] = operating_hours[-1] = 0
    in_out = diff(operating_hours)
    cumul_over_threshold = np.where(in_out == -1)[0] - np.where(in_out == 1)[0]

    nb_batches = (cumul_over_threshold - preheat_time) / baking_time

    in_out[in_out == 1] = nb_batches.astype(np.int16)
    in_out[in_out < 0] = 0

    return in_out


def _total_number_of_batches(dni, dni_threshold, preheat_time, baking_time):
    """ Return total nb of batches
    """

    operating_hours = (dni >= dni_threshold).astype(np.int16)
    operating_hours[0] = operating_hours[-1] = 0
    in_out = diff(operating_hours)
    cumul_over_threshold = np.where(in_out == -1)[0] - np.where(in_out == 1)[0]

    # Number of batches
    nb_batches = (cumul_over_threshold[cumul_over_threshold >= preheat_time]
                  - preheat_time) / baking_time

    return nb_batches.sum()
