class Oven:

    batch_size = None

    def __init__(self, oven_model):
        """ Implement the bakery's oven characteristics

        Parameters
        ----------
        oven_model: OvenModel
        """
        self.model = oven_model

    def batches(self, *args, **kwargs):
        """ Compute batches

        Parameters
        ----------
        args
        kwargs

        Returns
        -------

        """

        return self.model.number_of_batches(*args, **kwargs)


class Reflector:

    def __init__(self, reflector_model):
        """

        Parameters
        ----------
        reflector_model
        """

        self.model = reflector_model
