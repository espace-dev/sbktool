import os
from collections import OrderedDict

import geopandas as gpd
import numpy as np
import xarray as xr

from abc import abstractmethod

from pyrasta.raster import Raster

from sbk.utils import lazyproperty

XR_PARALLEL = True
XR_ENGINE = 'h5netcdf'


try:
    import h5netcdf
except ModuleNotFoundError:
    XR_ENGINE = 'netcdf4'


class GridDB:

    # Data main attribute
    _data = None

    # Geo features
    _crs = None
    _region = None

    # Time features
    _timedelta = None

    # Dimension names according to order (t, y, x)
    # _dims = (None, None, None)
    _dims = OrderedDict(t=None, y=None, x=None)

    def __add__(self, other):
        return self.build_from_data(self._data + other)

    def __mul__(self, other):
        return self.build_from_data(self._data * other)

    def __pow__(self, other):
        return self.build_from_data(self._data ** other)

    def __sub__(self, other):
        return self.build_from_data(self._data - other)

    def __radd__(self, other):
        return self.__add__(other)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __rsub__(self, other):
        return self.build_from_data(other - self._data)

    def __rpow__(self, other):
        return self.build_from_data(other ** self._data)

    def __truediv__(self, other):
        return self.build_from_data(self._data/other)

    def __rtruediv__(self, other):
        return self.build_from_data(other/self._data)

    @abstractmethod
    def clip(self, bounds=None, region=None, start_time=None,
             end_time=None, inplace=False, *args, **kwargs):
        pass

    @abstractmethod
    def resample(self,
                 resampling_rule=None,
                 resampler=None,
                 groupby=None,
                 groupby_method=None,
                 *args, **kwargs):
        """ Resample according to aggregating method

        Description
        -----------
        Resampling is performed before grouping

        Parameters
        ----------
        resampling_rule: str
            Rule to resample descriptor before computation
            {'D', 'M', 'Y', etc.}
        resampler: str or function
            Valid resampler function ("mean", "sum", etc.)
        groupby: str
            Temporal aggregation for resampling
            * "W": week
            * "M": month
            * "Y": year
        groupby_method: str
            Valid method to aggregate data by temporal similarities
            "mean", "sum", "min", "max", etc.
        args
        kwargs

        Returns
        -------

        """
        pass

    @abstractmethod
    def save(self, *args, **kwargs):
        """Save data to file
        """
        pass

    @classmethod
    @abstractmethod
    def build_from_data(cls, data, *args, **kwargs):
        pass

    @classmethod
    @abstractmethod
    def build_from_dir(cls, in_dir, var_name, *args, **kwargs):
        """ Build instance from directory

        Parameters
        ----------
        in_dir: str
            Input directory path OR absolute file path
        var_name: str
            Name of the variable stored in each file

        Returns
        -------

        """
        pass

    @classmethod
    @abstractmethod
    def build_from_list(cls, in_dir, var_names, *args, **kwargs):
        """ Build list of GridDB

        Parameters
        ----------
        in_dir: str
            Input directory path OR absolute file path
        var_names: list or tuple
            List of variable names, stored in each file
            within the directory

        """
        pass

    @lazyproperty
    def bounds(self):
        return _get_bounds(self)

    @property
    def crs(self):
        return self._crs

    @property
    def data(self):
        return self._data

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    def ndim(self):
        return self._data.ndim

    @property
    def region(self):
        return self._region

    @property
    @abstractmethod
    def latitude(self):
        pass

    @property
    @abstractmethod
    def longitude(self):
        pass

    @property
    def shape(self):
        return self._data.shape

    @property
    @abstractmethod
    def time(self):
        pass

    @property
    @abstractmethod
    def timedelta(self):
        pass

    @property
    def x_size(self):
        return self.longitude.size

    @property
    def y_size(self):
        return self.latitude.size


class RegularGridDB(GridDB):
    """RegularGridDB class definition
    a geo grid db is a regular grid database
    with cartographic outputs

    """
    _x_res = None
    _y_res = None

    def clip(self, bounds=None, region=None,
             start_time=None, end_time=None,
             inplace=False, *args, **kwargs):
        """ Clip DB geo boundaries

        Parameters
        ----------
        bounds: list or tuple or numpy.ndarray
            geo bounds as (minx, miny, maxx, maxy)_base_longitude
        region: str or geopandas.GeoDataFrame
            path to geo file
            OR
            GeoDataFrame (geopandas)
        start_time: str or datetime.datetime
        end_time: str or datetime.datetime
        inplace: bool
            if True, modify instance in place

        Returns
        -------

        """
        try:
            region = gpd.GeoDataFrame.from_file(region)
        except AttributeError:
            pass

        return _clip(self, bounds, region, start_time, end_time, inplace)

    def extract_timeseries(self, region, resampler, region_names=None):
        """ Extract timeseries from specified regions

        Description
        -----------
        Performs spatial resampling over GridDB
        going through time slices of the DB for
        each geometry located in the "region"
        layer. Thus, it returns time series for
        each geometry.

        Parameters
        ----------
        region: str or geopandas.GeoDataFrame
            path to geo file (shp, geojson, etc.)
            OR
            GeoDataFrame (geopandas)
        resampler: str
            Valid statistical method of DataArray
            object (see xarray doc) for spatial resampling
            "mean", "sum", "min", "max", etc.
        region_names: list of str
            Name of each geometry in region

        Returns
        -------
        list
            collection of pandas Series

        """

        try:
            region = gpd.GeoDataFrame.from_file(region)
        except AttributeError:
            pass

        return _zonal_stats(self, region, resampler, region_names)

    def resample(self,
                 resampling_rule=None,
                 resampler=None,
                 groupby=None,
                 groupby_method=None,
                 *args, **kwargs):
        """ Resample grid db along time dimension
        using specific function

        Parameters
        ----------
        resampling_rule: str
            valid offset alias, such as:
            H (hourly frequency)
            D (calendar day frequency)
            W (weekly frequency)
            M (month end frequency)
            Y (year end frequency).
        resampler: str
            Valid method of DataArrayResample object (see xarray doc)
            "mean", "sum", "min", "max", etc.
        groupby: str
            Valid str among: "Y" (year), "M" (month), "W" (week)
        groupby_method: str
            Valid method of DataArrayResample object (see xarray doc)
            "mean", "sum", "min", "max", etc.

        Returns
        -------

        """
        return _resample(self, resampling_rule, resampler, groupby, groupby_method)

    def save(self, filename,
             scale_factor=1,
             dtype="float64",
             no_data_value=None,
             *args, **kwargs):
        """ Save data to netcdf file

        Parameters
        ----------
        filename: str
            File path to write to
        scale_factor: int or float
            Data scale factor when stored
        dtype: str
            Data type ("int8", "int16", "float32", etc.)
        no_data_value: int or float
            If dtype is int, used to fill NaNs
        args
        kwargs

        Returns
        -------

        """
        encoding = {"dtype": dtype,
                    "scale_factor": scale_factor}

        if "float" not in dtype:
            encoding.update(_FillValue=no_data_value)

        self.data.to_netcdf(filename,
                            encoding={self.name: encoding})

    def to_raster(self, no_data=-999):
        """ Save Regular GridDB to raster

        Description
        -----------
        Be careful regarding the time dimension. If you
        do not resample the Grid DB before converting it
        to a raster, you may end up with a raster having
        far too much bands !

        Parameters
        ----------
        no_data: float or int
            No data value in output raster

        Returns
        -------

        """
        return _grid_db_to_raster(self,
                                  no_data)

    @classmethod
    def build_from_data(cls, data_array, *args, **kwargs):
        """ Build instance from already existing data array

        Parameters
        ----------
        data_array: xarray.DataArray

        Returns
        -------

        """
        return _build_from_data_array(cls, data_array)

    @classmethod
    def build_from_dir(cls, in_dir, var_name, extension=".nc", *args, **kwargs):
        """ Build instance from nc file(s)

        Parameters
        ----------
        in_dir: str
            Input directory path where nc files are stored
            OR
            Absolute path to nc file
        var_name: str
            Name of the variable stored in the dataset
        extension: str
            Extension of the files located in input directory

        Returns
        -------

        """
        return _build_from_dir(cls, in_dir, var_name, extension)

    @classmethod
    def build_from_list(cls, in_dir, var_names, extension=".nc", *args, **kwargs):
        """ Build list instances from list of variables

        Parameters
        ----------
        in_dir: str
            Input directory path where nc files are stored
            OR
            Absolute path to nc file
        var_names: list or tuple
            List of variable names, stored in the dataset
        extension: str
            file extension to search for in directory

        Returns
        -------

        """
        return _build_from_list(cls, in_dir, var_names, extension)

    @property
    def geo_transform(self):
        return (self.raster_bounds[0],
                self.x_res,
                0,
                self.raster_bounds[3],
                0,
                -self.y_res)

    @property
    def latitude(self):
        return self._data.__getitem__(self._dims["y"])

    @property
    def longitude(self):
        return self._data.__getitem__(self._dims["x"])

    @property
    def name(self):
        return self._data.name

    @property
    def raster_bounds(self):
        return (self.bounds[0] - self.x_res/2,
                self.bounds[1] - self.y_res/2,
                self.bounds[2] + self.x_res/2,
                self.bounds[3] + self.y_res/2)

    @property
    def time(self):
        return self._data.__getitem__(self._dims["t"])

    @lazyproperty
    def timedelta(self):  # Return time step in seconds
        if self._timedelta:
            return self._timedelta
        else:
            return (self.time[1] - self.time[0]).values.astype("float64") / 10**9

    @lazyproperty
    def x_res(self):
        if self._x_res:
            return self._x_res
        else:
            return (self.longitude[-1] - self.longitude[0]).values / (self.x_size - 1)

    @lazyproperty
    def y_res(self):
        if self._y_res:
            return self._y_res
        else:
            return (self.latitude[-1] - self.latitude[0]).values / (self.y_size - 1)


class SparseGridDB(GridDB):
    """SparseGridDB class definition
    a sparse grid DB is a simple set of locations
    with punctual variable outputs
    (ex.: weather stations/specific reanalysis/satellite pixels, etc.)

    """

    def build_from_dir(self, in_dir, var_name, *args, **kwargs):
        pass

    def clip(self, bounds=None, region=None,
             start_time=None, end_time=None,
             inplace=False, *args, **kwargs):
        pass

    def resample(self,
                 resampling_rule=None,
                 resampler=None,
                 groupby=None,
                 groupby_method=None,
                 *args, **kwargs):
        pass

    @property
    @abstractmethod
    def latitude(self):
        pass

    @property
    @abstractmethod
    def longitude(self):
        pass

    @property
    @abstractmethod
    def time(self):
        pass


def _build_from_data_array(grid_db_class, data_array):

    grid_db = grid_db_class()
    grid_db._data = data_array.transpose(*grid_db._dims.values())

    return grid_db


def _build_from_dir(grid_db_class, in_dir, var_name, extension):

    try:
        xr_dataset = xr.open_mfdataset(os.path.join(in_dir, '*' + extension),
                                       parallel=XR_PARALLEL,
                                       engine=XR_ENGINE)
    except OSError:
        xr_dataset = xr.open_dataset(in_dir)

    try:
        data_array = xr_dataset.__getattr__(var_name)
        return grid_db_class.build_from_data(data_array)
        # return _build_from_data_array(grid_db_class, data_array)
    except AttributeError:
        raise ValueError(f"Unknown variable name: '{var_name}'")
    finally:
        xr_dataset.close()


def _build_from_list(grid_db_class, in_dir, var_names, extension):

    list_of_grid_db = []

    for variable in var_names:
        list_of_grid_db.append(grid_db_class.build_from_dir(in_dir,
                                                            variable,
                                                            extension))
        # list_of_grid_db.append(grid_db_class(in_dir, variable))

    return list_of_grid_db


def _clip(grid_db, bounds, region, start_time, end_time, inplace):

    try:
        if region.crs != grid_db.crs:
            region = region.to_crs(crs=grid_db.crs)
    except AttributeError:
        pass

    if bounds is None:
        try:
            bounds = region.total_bounds
        except AttributeError:
            raise TypeError("Invalid type for 'region' input")

    if start_time is None:
        start_time = grid_db.time[0]

    if end_time is None:
        end_time = grid_db.time[-1]

    lat, lon = grid_db.latitude, grid_db.longitude

    # Set padding around geo boundaries while clipping
    try:
        minx = max(0, np.where(lon < bounds[0])[0][-1])
        maxx = min(np.where(lon > bounds[2])[0][0], lon.size)
        miny = max(0, np.where(lat < bounds[1])[0][-1])
        maxy = min(np.where(lat > bounds[3])[0][0], lat.size)
    except IndexError:
        raise ValueError("Requested bounds out of the Grid DB boundaries")

    if inplace:
        new_grid_db = grid_db
    else:  # Compute new object
        new_grid_db = grid_db.__class__()
        # new_grid_db = copy(grid_db)

    data_array = grid_db._data.isel({grid_db._dims["x"]: slice(minx, maxx),
                                     grid_db._dims["y"]: slice(miny, maxy),
                                     grid_db._dims["t"]: slice(start_time,
                                                               end_time)})
    new_grid_db = _build_from_data_array(grid_db.__class__, data_array)
    new_grid_db._region = region

    return new_grid_db


def _get_bounds(grid_db):

    return np.asarray([grid_db.longitude.min(), grid_db.latitude.min(),
                       grid_db.longitude.max(), grid_db.latitude.max()])


def _grid_db_to_raster(grid_db, out_no_data):

    # If latitude is sorted in ascending order
    # (from south to north), do not forget to
    # "reverse" the array
    if grid_db.latitude[0] < grid_db.latitude[-1]:
        values = grid_db._data.values[:, ::-1, :]  # dimension order --> (t, y, x)
    else:
        values = grid_db._data.values

    values[np.isnan(values)] = out_no_data

    return Raster.from_array(values,
                             crs=grid_db.crs,
                             bounds=grid_db.raster_bounds,
                             no_data=out_no_data)


# noinspection PyUnresolvedReferences
def _resample(grid_db, rule, resampler, groupby, method):

    if isinstance(grid_db, RegularGridDB):  # Do resampling for regular GridDB

        if rule and resampler:
            resampled = getattr(grid_db._data.resample({grid_db._dims["t"]: rule}), resampler)()
            # resampled = grid_db._data.resample({grid_db._t_name: rule}).__
            # getattribute__(resampler)()
        else:
            resampled = grid_db._data

        if groupby and method:
            if groupby == "Y":
                resampled = getattr(resampled.groupby(grid_db._dims["t"] + ".year"), method)()
                # resampled = resampled.groupby(grid_db._t_name + ".year").__
                # getattribute__(method)()
            elif groupby == "M":
                resampled = getattr(resampled.groupby(grid_db._dims["t"] + ".month"), method)()
                # resampled = resampled.groupby(grid_db._t_name + ".month").__
                # getattribute__(method)()
            elif groupby == "W":
                resampled = getattr(resampled.groupby(grid_db._dims["t"] + ".week"), method)()
                # resampled = resampled.groupby(grid_db._t_name + ".week").__
                # getattribute__(method)()
        else:
            pass

    else:  # Do resampling for sparse GridDB
        pass

    return grid_db.build_from_data(resampled)


def _zonal_stats(grid_db, region, stat, region_names):

    region["temp_id"] = range(len(region))
    mask = Raster.rasterize(region,
                            grid_db.x_size,
                            grid_db.y_size,
                            grid_db.geo_transform,
                            attribute="temp_id").read_array()

    if grid_db.latitude[0] < grid_db.latitude[-1]:  # Means lat is reversed (from south to north) !
        mask = mask[::-1, :]

    data = []
    for _id in region["temp_id"]:
        masked_data = grid_db.data.where(mask == _id)
        ts = getattr(masked_data, stat)(dim=(grid_db._dims["y"],
                                             grid_db._dims["x"]),
                                        skipna=True)

        data.append(ts.to_series())

    if region_names:
        for series, name in zip(data, region_names):
            series.name = name

    return data
