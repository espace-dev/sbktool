from collections import OrderedDict

from pyproj import CRS

from sbk.grid_db.base import RegularGridDB


class Sarah3(RegularGridDB):

    _crs = CRS(4326)  # Coordinate Reference System

    # Dimension names following Mother Class order
    _dims = OrderedDict(t="time", y="lat", x="lon")

    _timedelta = 1800  # Time step in seconds
    _x_res = 0.05  # lat resolution in degrees
    _y_res = 0.05  # lon resolution in degrees
