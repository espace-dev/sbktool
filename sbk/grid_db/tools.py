import os
from copy import copy

import h5py
import netCDF4 as nc
import numpy as np
import pyproj
from pyrasta.raster import Raster

from sbk.iotools import find_file


DO_SHADING = "do_shading"
BOUNDS = "bounds"
DEM = "dem"
NO_DATA = "no_data"
PADDING = "padding"
REGION = "region"
EPSG = "epsg"


def _build_sarah2(sarah2, directory):
    sarah2._files = find_file(".nc", directory)
    sarah2._dataset = [nc.Dataset(file, 'r') for file in sarah2._files]
    sarah2._base_latitude = sarah2._dataset[0].variables["lat"][:]
    sarah2._base_longitude = sarah2._dataset[0].variables["lon"][:]
    sarah2._bounds = (min(sarah2._base_longitude),
                      min(sarah2._base_latitude),
                      max(sarah2._base_longitude),
                      max(sarah2._base_latitude))
    sarah2._bounds_idx = (0, 0, sarah2._base_longitude.size, sarah2._base_latitude.size)

    sarah2.directory = directory

    return 0
    # return sarah2


def _load_solar_db(solar_db, *args, **kwargs):

    with h5py.File(solar_db.h5, 'r') as h5:
        solar_db.do_shading = h5.attrs[DO_SHADING]

        solar_db.build_from_dir(*args, **kwargs)

        try:
            solar_db = solar_db.clip(region=h5.attrs[REGION], inplace=True)
        except KeyError:
            solar_db = solar_db.clip(bounds=h5.attrs[BOUNDS], inplace=True)

        try:
            dem = Raster.from_array(h5[DEM][:],
                                    pyproj.CRS.from_epsg(h5[DEM].attrs[EPSG]),
                                    h5[DEM].attrs[BOUNDS],
                                    no_data=h5[DEM].attrs[NO_DATA])
            solar_db._fetch_dem(dem, h5[DEM].attrs[PADDING])
        except KeyError:
            pass

    return 0


def _save_solar_db(solar_db):
    out_dir, h5_file = os.path.split(solar_db.h5)
    file_name = os.path.splitext(h5_file)[0]
    region_file = os.path.join(out_dir, file_name + ".geojson")

    with h5py.File(solar_db.h5, 'r+') as h5:
        h5.attrs[DO_SHADING] = solar_db.do_shading
        h5.attrs[BOUNDS] = solar_db.bounds

        # Save DEM as a dataset array
        try:
            h5[DEM] = solar_db._padded_dem.read_array()
            h5[DEM].attrs[EPSG] = solar_db._padded_dem.crs.to_epsg()
            h5[DEM].attrs[PADDING] = solar_db.dem_padding
            h5[DEM].attrs[BOUNDS] = solar_db._padded_dem.bounds
            h5[DEM].attrs[NO_DATA] = solar_db._padded_dem.no_data
        except AttributeError:
            pass

        if solar_db._region_file:
            h5.attrs[REGION] = solar_db._region_file
        else:
            # Save region as a geojson file if necessary
            try:
                solar_db.region.to_file(region_file, driver="GeoJSON")
                h5.attrs[REGION] = region_file
            except AttributeError:
                pass

    return 0
