import geopandas as gpd
import multiprocessing as mp

from sbk.exceptions import PopDensityError
from sbk.population.grid import mesh_geodataframe
from sbk.population.tools import pop_from_meta, build_density_map


class PopDensity:

    grid_spacing = None
    grid = None

    def __init__(self, region):
        """ Build PopDensity instance

        Parameters
        ----------
        region: str or geopandas.GeoDataFrame
            Region for which pop density map must be computed
        """
        try:
            self.region = gpd.GeoDataFrame.from_file(region)
        except AttributeError:
            self.region = region

    def create_mesh(self, grid_spacing, do_intersection=True,
                    split_before_intersect=False,
                    nb_processes=mp.cpu_count()//2):
        """ Create grid mesh

        Parameters
        ----------
        grid_spacing: int or float
            Resolution of the output mesh
        do_intersection: bool
            if True, fit the grid mesh to the region
            (cells that intersect with boundaries are cut)
            if False, return complete grid cells
            that intersect with the region
        split_before_intersect: bool
            if True, fasten the process.
            If True, do_intersection should be False
        nb_processes

        Returns
        -------

        """
        self.grid_spacing = grid_spacing
        self.grid = mesh_geodataframe(self.region,
                                      self.grid_spacing,
                                      do_intersection,
                                      split_before_intersect,
                                      nb_processes)

        return self

    def from_meta(self, ref_country, output_field="POPULATION",
                  method="sum", nb_processes=mp.cpu_count()//2):
        """ Retrieve pop density map from online Meta (Facebook) data

        Description
        -----------
        Retrieve pop density from online Meta (Facebook)
        "Data for Good" high resolution population density
        maps located on the Humanitarian Data Exchange (HDX)

        Parameters
        ----------
        ref_country: str
            Reference country for HDX import
        output_field: str
            name of pop field in output geodataframe
        method: str
            method used to compute zonal stat in each cell
        nb_processes: int
            Number of processes for zonal_stat multiprocessing

        Returns
        -------
        geopandas.GeoDataFrame

        """
        pop_raster = pop_from_meta(ref_country)

        return self.from_raster(pop_raster,
                                output_field,
                                method,
                                nb_processes)

    def from_raster(self, pop_raster,
                    output_field="POPULATION",
                    method="sum", nb_processes=mp.cpu_count()//2):
        """ Retrieve pop density map from pop count raster

        Description
        -----------
        Build population density grid based on population
        raster zonal stat (default method: "sum")

        Parameters
        ----------
        pop_raster: pyrasta.raster.Raster
            population raster data
        output_field: str
            name of pop field in output geodataframe
        method: str
            method used to compute zonal stat in each cell
        nb_processes: int
            Number of processes for zonal_stat multiprocessing

        Returns
        -------
        geopandas.GeoDataFrame

        """
        if self.grid is None:
            raise PopDensityError("Must define a mesh before computing density map")

        return build_density_map(pop_raster,
                                 self.grid,
                                 output_field,
                                 method,
                                 nb_processes)
