import os
import re

import geopandas as gpd
from pyrasta.raster import Raster
from tqdm import tqdm

from sbk.iotools import find_file
from sbk.population.base import PopDensity

puy_de_dome = "/media/benjamin/STORAGE/BOUSOLRUN/DEPARTEMENTS/puy_de_dome.geojson"
france_metro = "/media/benjamin/STORAGE/BOUSOLRUN/DEPARTEMENTS/france_metro_no_islands.geojson"
in_dir = "/media/benjamin/STORAGE/BOUSOLRUN/POPULATION_DENSITY/FRA_100m_Dynamic_Population"
out_dir = "/media/benjamin/STORAGE/BOUSOLRUN/POPULATION_DENSITY/POP_DENSITY_DYNAMICS"
# region = gpd.GeoDataFrame.from_file(france_metro)
spacing = 2  # in km

france_raster = Raster("/media/benjamin/STORAGE/BOUSOLRUN/POPULATION_DENSITY/population_fra_2019-07-01_geotiff"
                       "/population_fra_2019-07-01.tif")
region = gpd.GeoDataFrame.from_file(puy_de_dome)
pop = PopDensity(region)
pop = pop.create_mesh(1000, nb_processes=12)
grid = pop.from_raster(france_raster)

grid.to_file("/media/benjamin/STORAGE/BOUSOLRUN/POPULATION_DENSITY/testpdd.geojson",
             driver="GeoJSON")

# pop = PopDensity(region)
# pop.create_mesh(spacing * 1000, nb_processes=24)
#
# files = [file for file in find_file(".tif", in_dir) if "_ppp_" in file]
#
# for file in tqdm(files):
#     pop_density = pop.from_raster(Raster(file), nb_processes=24)
#     filename = os.path.split(file)[1]
#     month = re.search(r"\d+-\d+", filename)[0]
#     pop_density.to_file(os.path.join(out_dir, f"pop_density_{spacing}km_{month}.geojson"),
#                         driver="GeoJSON")
