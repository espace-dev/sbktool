import geopandas as gpd

from functools import partial

import multiprocessing as mp
import numpy as np
from numba import njit
from pandas import concat
from shapely import box, GeometryCollection, MultiPolygon
from shapely.geometry import Polygon
from tqdm import tqdm

from sbk.utils import is_iterable, r_tree_idx


# Value used in _katana split of geometry;
# threshold is based on number of entities
KATANA_THRESHOLD_AS_NB_OF_ENTITIES = 200


def mesh_geodataframe(geodataframe, spacing, do_intersection,
                      split_before_intersect, nb_processes):
    """ Split geodataframe into honeycomb mesh

    Parameters
    ----------
    geodataframe
    spacing
    do_intersection: bool
    split_before_intersect: bool
    nb_processes

    Returns
    -------

    """
    new_geometry = []
    df = []
    radius = spacing / 2
    for i, geometry in enumerate(geodataframe.geometry):
        new_geom = _hexana(geometry, radius, do_intersection,
                           split_before_intersect, nb_processes)
        new_geometry.extend(new_geom)
        df.extend([geodataframe.iloc[[i]]] * len(new_geom))

    return gpd.GeoDataFrame(concat(df, ignore_index=True),
                            geometry=new_geometry,
                            crs=geodataframe.crs)


def _explode(geometry_collection):
    """ Convert multipart geometry collection into single-part

    :param geometry_collection: valid geometry collection
    :return:
    """
    single = []
    if not is_iterable(geometry_collection):
        geometry_collection = [geometry_collection]

    for geom in geometry_collection:
        try:
            single.extend(geom)
        except TypeError:
            single.append(geom)

    return single


def _hexana(polygon, radius, do_intersection,
            do_katana, nb_processes):
    """ Split polygon into sub-hexagons

    Parameters
    ----------
    polygon
    radius
    do_intersection
    do_katana
    nb_processes

    Returns
    -------

    """
    if do_intersection:
        fhandle = _intersect2
    else:
        fhandle = _intersect

    meshes = []
    r_tree = None

    grid = list(_honeycomb(*polygon.bounds, radius=radius))

    if do_katana:
        r_tree = r_tree_idx(grid)
        hexagon_area = 2 * np.sqrt(3) * radius ** 2
        threshold_area = min(polygon.area, KATANA_THRESHOLD_AS_NB_OF_ENTITIES * hexagon_area)
        sub_polygons = _katana(polygon, threshold_area)
    else:
        sub_polygons = [polygon]

    for poly in tqdm(sub_polygons, desc="Intersecting grid with main geometry"):
        try:
            sub_grid = [grid[idx] for idx in r_tree.intersection(poly.bounds)]
        except AttributeError:
            sub_grid = grid

        with mp.Pool(processes=nb_processes) as pool:
            meshes.extend(list(pool.map(partial(fhandle, polygon2=poly),
                                        sub_grid, chunksize=None)))

    return [geom.buffer(0) for geom in meshes if geom is not None]


# Thanks to https://gist.github.com/urschrei/17cf0be92ca90a244a91
@njit()
def _honeycomb_nb(startx, starty, endx, endy, radius):
    """
    Calculate a grid of hexagon coordinates of the given radius
    given lower-left and upper-right coordinates
    Returns a list of lists containing 6 tuples of x, y point coordinates
    These can be used to construct valid regular hexagonal polygons

    - update 04/23/2019:
        * can give either radius or area of unit hexagon
        * return a list of shapely Polygon

    You will probably want to use projected coordinates for this
    """

    # calculate side length given radius
    sl = (2 * radius) * np.tan(np.pi / 6)
    # calculate radius for a given side-length
    # (a * (math.cos(math.pi / 6) / math.sin(math.pi / 6)) / 2)
    # see http://www.calculatorsoup.com/calculators/geometry-plane/polygon.php

    # calculate coordinates of the hexagon points
    # sin(30)
    p = sl * 0.5
    b = sl * np.cos(np.radians(30))
    w = b * 2
    h = 2 * sl

    # offset start and end coordinates by hex widths and heights to guarantee coverage
    startx = startx - w
    starty = starty - h
    endx = endx + w
    endy = endy + h
    origx = startx

    # offsets for moving along and up rows
    xoffset = b
    yoffset = 3 * p

    row = 1

    while starty < endy:
        if row % 2 == 0:
            startx = origx + xoffset
        else:
            startx = origx
        while startx < endx:
            p1x = startx
            p1y = starty + p
            p2x = startx
            p2y = starty + (3 * p)
            p3x = startx + b
            p3y = starty + h
            p4x = startx + w
            p4y = starty + (3 * p)
            p5x = startx + w
            p5y = starty + p
            p6x = startx + b
            p6y = starty
            poly = [
                (p1x, p1y),
                (p2x, p2y),
                (p3x, p3y),
                (p4x, p4y),
                (p5x, p5y),
                (p6x, p6y),
                (p1x, p1y)]
            yield poly
            startx += w
        starty += yoffset
        row += 1


def _honeycomb(startx, starty, endx, endy, radius=None, area=None):
    """

    Parameters
    ----------
    startx
    starty
    endx
    endy
    radius
    area

    Returns
    -------

    """

    if not radius:
        radius = np.sqrt(area / (2*np.sqrt(3)))

    return (Polygon(poly) for poly in _honeycomb_nb(startx, starty, endx, endy, radius))


def _intersect(polygon1, polygon2):
    if polygon1.intersects(polygon2):
        return polygon1
    # elif polygon1.overlaps(polygon2):
    #     return polygon1.intersection(polygon2)


def _intersect2(polygon1, polygon2):
    if polygon1.within(polygon2):
        return polygon1
    elif polygon1.overlaps(polygon2):
        return polygon1.intersection(polygon2)


def _katana(polygon, threshold, count=0):
    """ Split a polygon

    See https://snorfalorpagus.net/blog/2016/03/13/splitting-large-polygons-for-faster
    -intersections/

    Copyright (c) 2016, Joshua Arnott

    All rights reserved.

    Redistribution and use in source and binary forms, with or without modification,
    are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this list of
    conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, this list of
    conditions and the following disclaimer in the documentation and/or other materials provided
    with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
    OR  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
    OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
     OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
     POSSIBILITY OF SUCH DAMAGE.

    :param polygon: Shapely polygon
    :param threshold:
    :param count:
    :return:
    """
    if count == 0:
        if not polygon.is_valid:
            polygon = polygon.buffer(0, 0)

    result = []
    width = polygon.bounds[2] - polygon.bounds[0]
    height = polygon.bounds[3] - polygon.bounds[1]
    if width * height <= threshold or count == 250:
        return [polygon]
    if height >= width:
        a = box(polygon.bounds[0], polygon.bounds[1],
                polygon.bounds[2], polygon.bounds[1] + height/2)
        b = box(polygon.bounds[0], polygon.bounds[1] + height/2,
                polygon.bounds[2], polygon.bounds[3])
    else:
        a = box(polygon.bounds[0], polygon.bounds[1],
                polygon.bounds[0] + width/2, polygon.bounds[3])
        b = box(polygon.bounds[0] + width/2, polygon.bounds[1],
                polygon.bounds[2], polygon.bounds[3])

    for sword in (a, b,):
        split_poly = polygon.intersection(sword)
        if not isinstance(split_poly, GeometryCollection):
            split_poly = [split_poly]
        for sub_poly in split_poly:
            if isinstance(sub_poly, (Polygon, MultiPolygon)):
                result.extend(_katana(sub_poly, threshold, count+1))

    return result
