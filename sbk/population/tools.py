import os
import tempfile
from urllib.error import URLError
from urllib.request import urlretrieve
from zipfile import ZipFile

import pycountry
from hdx.api.configuration import Configuration
from hdx.data.dataset import Dataset
from pyrasta.raster import Raster
from pyrasta.utils import TqdmUpTo
from tqdm import tqdm

from sbk.iotools import find_file

META_DS_NAME_1 = "-high-resolution-population-density-maps-demographic-estimates"
META_DS_NAME_2 = "highresolutionpopulationdensitymaps-"
GEOTIFF_EXT = ".tif"
NO_VALID = ("children", "elderly", "women", "men", "youth")


def build_density_map(pop_raster, grid, field_name,
                      zonal_stat_method, nb_processes):
    """ Build population density map from raster data

    Parameters
    ----------
    pop_raster: pyrasta.raster.Raster
    grid: geopandas.GeoDataFrame
        Mesh of the region for which pop count must be computed
    field_name: str
        Name of population field in resulting GeoDataFrame
    zonal_stat_method: str
        Method used to compute population in corresponding cell
    nb_processes

    Returns
    -------

    """
    pop_grid = grid.to_crs(pop_raster.crs)
    pop_raster = pop_raster.clip(bounds=pop_grid.total_bounds)
    population = pop_raster.zonal_stats(pop_grid,
                                        stats=[zonal_stat_method],
                                        nb_processes=nb_processes)
    pop_grid.insert(len(grid.columns) - 1, field_name, population[zonal_stat_method])

    return pop_grid.dropna(subset=[field_name]).to_crs(grid.crs)


def hdx_import(country_name, out_dir):
    """ Import Meta pop data from the Humanitarian Data Exchange (HDX)

    Parameters
    ----------
    country_name
    out_dir

    Returns
    -------

    """
    Configuration.create(hdx_site="prod", user_agent="MyInstitution_pop", hdx_read_only=True)
    cname = pycountry.countries.search_fuzzy(country_name)[0]

    if cname:
        data = Dataset.read_from_hdx(cname.name.lower().replace(" ", "-") + META_DS_NAME_1)

        try:
            resources = data.resources[0]
        except AttributeError:
            try:
                data = Dataset.read_from_hdx(META_DS_NAME_2 + cname.alpha_3.lower())
            except AttributeError:
                raise RuntimeError("Pop data for this country is not available from HDX")

        ds_names = [resource.data['name'] for resource in data.resources]
        valid_resources = [resource for resource, ds_name in zip(data.resources, ds_names)
                           if "geotiff" in ds_name and
                           all([name not in ds_name for name in NO_VALID])]

    else:
        valid_resources = []

    for resource in tqdm(valid_resources,
                         desc=f"Downloading population data for '{cname.name}'"):
        temp_file_zip = os.path.join(tempfile.gettempdir(), resource.data['name'])
        try:
            with TqdmUpTo(unit='B', unit_scale=True, unit_divisor=1024, miniters=1,
                          desc=f"Downloading {resource.data['name']}") as t:
                urlretrieve(resource.data['url'], temp_file_zip, reporthook=t.update_to)
                t.total = t.n
        except URLError as e:
            raise RuntimeError("Unable to fetch data at '%s': %s" % (resource.data['url'], e))

        with ZipFile(temp_file_zip) as zip_obj:

            zip_obj.extractall(path=out_dir)

    return 0


def pop_from_meta(country_name):
    """ Build population raster from online Meta data

    Parameters
    ----------
    country_name: str
        Valid country name

    Returns
    -------
    pyrasta.raster.Raster

    """
    temp_dir = tempfile.mkdtemp()
    hdx_import(country_name, temp_dir)
    geotiff_file = find_file(GEOTIFF_EXT, temp_dir)

    if len(geotiff_file) > 1:
        pop_raster = Raster.merge([Raster(file) for file in geotiff_file])
    else:
        pop_raster = Raster(geotiff_file[0])

    return pop_raster
