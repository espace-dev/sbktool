from pyrasta.exceptions import RasterBaseError
from pyrasta.raster import DigitalElevationModel
from pyrasta.tools.srtm import from_cgiar_online_database


def _fetch_dem(from_, region, bounds, dem_pad):
    """ Fetch DEM corresponding to the area from online databases

    Parameters
    ----------
    from_: str or pyrasta.raster.DigitalElevationModel
        online database DEM data must be retrieved from
    region
    bounds
    dem_pad

    Returns
    -------

    """

    pad_bounds = (bounds[0] - dem_pad,
                  bounds[1] - dem_pad,
                  bounds[2] + dem_pad,
                  bounds[3] + dem_pad)

    if from_ == "SRTM3":
        padded_dem = from_cgiar_online_database(pad_bounds)
    else:
        try:
            padded_dem = DigitalElevationModel(from_)
        except RasterBaseError:
            padded_dem = from_
            # raise ValueError(f"'{from_}' is not a valid raster file")
        # TODO: we may implement other methods for fetching DEMs

    try:
        bounded_dem = padded_dem.clip(mask=region,
                                      no_data=padded_dem.no_data)
    except ValueError:
        bounded_dem = padded_dem.clip(bounds)

    return padded_dem, bounded_dem
