import multiprocessing as mp
import os
from functools import partial
from importlib import reload
from pvlib import spa

import h5py
import numpy as np

from pyrasta.utils import split_into_chunks
from tqdm import tqdm

from sbk import HORIZON_VS_SUN_DTYPE
from sbk.iotools import H5File

os.environ['PVLIB_USE_NUMBA'] = '1'
reload(spa)


def get_position(location, time, h5_path, ds_name, no_data, pressure,
                 temperature, delta_t, atmos_refrac):

    horizon_vs_sun = np.ones(time.size, dtype=HORIZON_VS_SUN_DTYPE)

    if location[2] != no_data:
        with h5py.File(h5_path, 'r') as h5:
            horizon = h5[ds_name][location[3], :]

        _, _, _, elevation, azimuth, _ = spa.solar_position(time, location[0],
                                                            location[1], location[2],
                                                            pressure, temperature,
                                                            delta_t, atmos_refrac)

        # sun_pos = pvlib.solarposition.spa_python(time, location[0], location[1],
        #                                          altitude=location[2], how="numba")
        horizon_vs_sun[elevation <= horizon[np.round(azimuth).astype(int)]] = 0

    return horizon_vs_sun


def compute_horizon_vs_sun(longitude, latitude, altitude, time, h5_path, h5_ds_name,
                           h5_horizon_ds_name, dem_no_data, pressure, temperature, delta_t,
                           atmos_refrac, nb_processes, chunk_size):
    """ Compute sun shading effects due to horizon

    Parameters
    ----------
    longitude
    latitude
    altitude
    time
    h5_path
    h5_ds_name
    h5_horizon_ds_name
    dem_no_data
    pressure
    temperature
    delta_t
    atmos_refrac
    nb_processes
    chunk_size

    Returns
    -------

    """
    location_generator = ((lat, lon, alt, n) for n, (lat, lon, alt)
                          in enumerate(zip(latitude, longitude, altitude)))

    unixtime = np.array(time.view(np.int64) / 10 ** 9)

    # H5FILE
    with H5File(h5_path) as h5:
        h5.reset_dataset(h5_ds_name,
                         shape=(altitude.size, time.size),
                         dtype=HORIZON_VS_SUN_DTYPE)

        # Processing
        pg = tqdm(total=altitude.size // chunk_size + 1,
                  desc="Compute horizon vs sun positions")

        for _, loc_generator in enumerate(split_into_chunks(location_generator,
                                                            chunk_size)):
            with mp.Pool(processes=nb_processes) as pool:
                h_vs_s = np.asarray(list(pool.map(partial(get_position,
                                                          time=unixtime,
                                                          h5_path=h5_path,
                                                          ds_name=h5_horizon_ds_name,
                                                          no_data=dem_no_data,
                                                          pressure=pressure,
                                                          temperature=temperature,
                                                          delta_t=delta_t,
                                                          atmos_refrac=atmos_refrac),
                                                  loc_generator, chunksize=None)))

            h5.append(h5_ds_name, h_vs_s)
            pg.update(1)
        pg.close()

    return 0
