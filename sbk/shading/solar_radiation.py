import multiprocessing as mp
from functools import partial

import h5py
import numpy as np
from pyrasta.utils import split_into_chunks
from scipy.interpolate import RegularGridInterpolator
from tqdm import tqdm

from sbk.iotools import H5File


def _dem_vs_rad(dem_grid_x, dem_grid_y, lat_rad, lon_rad):
    """ Return solar rad pixels corresponding to DEM pixels
    
    Parameters
    ----------
    dem_grid_x
    dem_grid_y
    lat_rad: numpy.ndarray
        latitude coordinate array
    lon_rad: numpy.ndarray

    Returns
    -------

    """

    lon_dem, lat_dem = np.meshgrid(dem_grid_x, dem_grid_y)
    pixel_rad = np.arange(lat_rad.size * lon_rad.size).reshape(lat_rad.size,
                                                               lon_rad.size)

    try:
        interp = RegularGridInterpolator((lat_rad, lon_rad),
                                         pixel_rad,
                                         method="nearest")
    except ValueError:
        # If latitude is not in ascending order,
        # reverse corresponding radiation pixel order
        interp = RegularGridInterpolator((lat_rad[::-1], lon_rad),
                                         pixel_rad[::-1, :],
                                         method="nearest")

    return interp((lat_dem, lon_dem))


def _get_shaded_dni(row, dni, h5_path, h5_horizon_vs_sun_ds_name):
    """

    Parameters
    ----------
    dni
    h5_path
    h5_horizon_vs_sun_ds_name

    Returns
    -------

    """
    with h5py.File(h5_path) as h5:
        sh_dni = dni[row, :] * h5[h5_horizon_vs_sun_ds_name][row, :]

    return sh_dni


def _shaded_dni_sparse(dni, h5_path, h5_shaded_dni_ds_name,
                       h5_horizon_vs_sun_ds_name,
                       nb_processes, chunk_size):

    generator = (n for n in range(dni.shape[0]))

    with H5File(h5_path) as h5:

        h5.reset_dataset(h5_shaded_dni_ds_name,
                         dni.shape,
                         dtype=np.int16)

        # Processing
        pg = tqdm(total=dni.shape[0] // chunk_size + 1,
                  desc="Compute horizon vs sun positions")

        for _, gen in enumerate(split_into_chunks(generator,
                                                  chunk_size)):
            with mp.Pool(processes=nb_processes) as pool:
                sh_dni = np.asarray(list(pool.map(
                    partial(_get_shaded_dni,
                            dni=dni,
                            h5_path=h5_path,
                            h5_horizon_vs_sun_ds_name=h5_horizon_vs_sun_ds_name),
                    gen, chunksize=None)))

            h5.append(h5_shaded_dni_ds_name, sh_dni)
            pg.update(1)
        pg.close()

    return 0


def _dni_sparse():
    pass


def _shaded_dni_grid(dni, bounded_dem, time, lat_dni, lon_dni,
                     h5_path, h5_shaded_dni_ds_name, h5_horizon_vs_sun_ds_name):
    """ Compute shaded DNI

    Parameters
    ----------
    dni
    bounded_dem: pyrasta.raster.DigitalElevationModel
    lat_dni: numpy.ndarray
        latitude coordinate array
    lon_dni: numpy.ndarray
    h5_path

    Returns
    -------

    """

    # DNI vs DEM pixels
    pixel_dni = _dem_vs_rad(bounded_dem.grid_x,
                            bounded_dem.grid_y,
                            lat_dni,
                            lon_dni).ravel().astype(int)
    set_of_pixels = np.unique(pixel_dni)

    # Process
    with H5File(h5_path) as h5:

        h5.reset_dataset(h5_shaded_dni_ds_name,
                         (bounded_dem.x_size * bounded_dem.y_size, time.size),
                         dtype=np.int16)
        # h5[h5_shaded_dni_ds_name].attrs["grid_x"] = bounded_dem.grid_x
        # h5[h5_shaded_dni_ds_name].attrs["grid_y"] = bounded_dem.grid_y

        for pixel in tqdm(set_of_pixels, desc="Compute Shaded DNI"):

            y, x = np.unravel_index(pixel, (lat_dni.size,
                                            lon_dni.size))
            dni_over_pixel = dni[:, y, x]
            idx = np.argwhere(pixel_dni == pixel).ravel()

            h5[h5_shaded_dni_ds_name][idx, :] = \
                dni_over_pixel * h5[h5_horizon_vs_sun_ds_name][idx, :]

    return 0


def _dni_grid(dni, bounds, time, latitude, longitude,
              h5_path, h5_dni_ds_name, chunk_size):
    """

    Parameters
    ----------
    dni: numpy.ndarray
        DNI data corresponding to latitude
        and longitude
        MUST correspond to latitude order (ascending or descending)
    bounds
    time
    latitude
    longitude
    h5_path
    h5_dni_ds_name
    chunk_size

    Returns
    -------

    """
    dni = np.moveaxis(dni, 0, -1)

    # Invert DNI array if latitude in ascending order
    if latitude[-1] > latitude[0]:
        dni = dni[::-1]

    # Process
    with H5File(h5_path) as h5:
        h5.reset_dataset(h5_dni_ds_name,
                         (latitude.size * longitude.size, time.size),
                         dtype=np.int16)
        # h5[h5_dni_ds_name].attrs["grid_x"] = longitude
        # h5[h5_dni_ds_name].attrs["grid_y"] = latitude

        for pixel in tqdm(split_into_chunks(range(latitude.size * longitude.size), chunk_size),
                          desc="Compute DNI", total=(latitude.size * longitude.size)//chunk_size + 1):
            idx = list(pixel)
            y, x = np.unravel_index(idx, (latitude.size,
                                          longitude.size))
            dni_over_pixel = dni[y, x, :]

            h5[h5_dni_ds_name][idx, :] = dni_over_pixel

    return 0
