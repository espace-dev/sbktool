from functools import wraps
from itertools import chain, islice

from numba import jit
from rtree import index


# Class decorator to apply constraints
def force_type(**kwargs):
    def decorate(cls):
        for key, value in kwargs.items():
            setattr(cls, key, value(key))
        return cls

    return decorate


def typed_property(name, expected_type):
    storage_name = '_' + name

    @property
    def prop(self):
        return getattr(self, storage_name)

    @prop.setter
    def prop(self, value):
        if not isinstance(value, expected_type):
            raise TypeError(f'{name} must be {expected_type}')
        setattr(self, storage_name, value)

    return prop


@jit(nopython=True, nogil=True)
def get_block_windows(window_size, raster_x_size, raster_y_size):
    """ Get block window coordinates

    Get block window coordinates depending
    on raster size and window size

    Parameters
    ----------
    window_size: int
        window size
    raster_x_size: int
        raster width
    raster_y_size: int
        raster height

    Returns
    -------

    """
    for y in range(0, raster_y_size, window_size):
        ysize = min(window_size, raster_y_size - y)
        for x in range(0, raster_x_size, window_size):
            xsize = min(window_size, raster_x_size - x)

            yield x, y, xsize, ysize


def get_slice_along_axis(ndim, axis, _slice):
    """ Used to make indexing with any n-dimensional numpy array

    Parameters
    ----------
    ndim: int
        number of dimensions
    axis: int
        axis for which we want the slice
    _slice: slice
        the required slice

    Returns
    -------
    """
    slc = [slice(None)] * ndim
    slc[axis] = _slice

    return tuple(slc)


def intersecting_features(geometry, geometry_collection, r_tree):
    """ Return list of geometries intersecting with given geometry

    Parameters
    ----------
    geometry
    geometry_collection
    r_tree

    Returns
    -------

    """
    is_intersecting = intersects(geometry, geometry_collection, r_tree)
    return [i for i in range(len(geometry_collection)) if is_intersecting[i]], \
           [geom for i, geom in enumerate(geometry_collection) if is_intersecting[i]]


def intersects(geometry, geometry_collection, r_tree):
    """ Return if geometry intersects with geometries in collection

    Parameters
    ----------
    geometry
    geometry_collection
    r_tree

    Returns
    -------

    """
    if r_tree is None:
        r_tree = r_tree_idx(geometry_collection)

    list_of_intersecting_features = list(r_tree.intersection(geometry.bounds))

    return [False if f not in list_of_intersecting_features
            else geometry.intersects(geometry_collection[f]) for f in
            range(len(geometry_collection))]


def is_iterable(iterable):
    """

    Parameters
    ----------
    iterable

    Returns
    -------

    """
    try:
        iter(iterable)
        return True
    except TypeError:
        return False


def lazyproperty(func):
    name = '_lazy_' + func.__name__

    @property
    def lazy(self):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            value = func(self)
            setattr(self, name, value)
            return value
    return lazy


def r_tree_idx(geometry_collection):
    """ Return Rtree spatial index of geometry collection

    Parameters
    ----------
    geometry_collection

    Returns
    -------

    """
    idx = index.Index()
    if not is_iterable(geometry_collection):
        geometry_collection = [geometry_collection]

    for i, geom in enumerate(geometry_collection):
        idx.insert(i, geom.bounds)

    return idx


def split_into_chunks(iterable, size):
    """ Split iterable into chunks of iterables

    """
    iterator = iter(iterable)
    for first in iterator:
        yield chain([first], islice(iterator, size - 1))


def this_is(obj_class):

    def decorator(setter):
        @wraps(setter)
        def _is_class(self, obj):
            if not isinstance(obj, obj_class):
                raise TypeError(f"'{setter.__name__}' must be "
                                f"'{obj_class}' but is "
                                f"'{obj.__class__.__name__}' instance")
            output = setter(self, obj)

        return _is_class
    return decorator
