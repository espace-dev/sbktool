import os

import geopandas as gpd
import numpy as np
from pyrasta.raster import Raster

from sbk.analysis.descriptors import Batches
from sbk.bakery.models import SolarThresholdModel
from sbk.bakery.oven import Oven
from sbk.population.base import PopDensity
from sbk.solar_db.sarah import Sarah2


def week_strategy(x):
    return np.sum(np.sort(x)[-2:])


puy_saint_andre = "/media/benjamin/STORAGE/BOUSOLRUN/DEPARTEMENTS/puy_saint_andre.geojson"
directory = "/media/benjamin/STORAGE/BOUSOLRUN/CMSAF/FRANCE_2018_2022"
out_dir = "/media/benjamin/STORAGE/BOUSOLRUN/H5"
result_dir = "/media/benjamin/STORAGE/BOUSOLRUN/RESULTS/PUY_SAINT_ANDRE"
pop_out_dir = "/media/benjamin/STORAGE/BOUSOLRUN/POPULATION_DENSITY"

rname = "puy_saint_andre"

h5file = os.path.join(out_dir, rname)
h5_to = os.path.join(result_dir, "descriptors")

oven = Oven(SolarThresholdModel(700, 2, 1))

# Population
france_pop = Raster("/media/benjamin/STORAGE/BOUSOLRUN/POPULATION_DENSITY/population_fra_2019-07-01_geotiff"
                    "/population_fra_2019-07-01.tif")

# Computation
# solar_db = Sarah2(directory, h5file)
# new_solar_db = solar_db.clip(region=puy_saint_andre)
# new_solar_db.compute_terrain(distance=0.1)
# new_solar_db.compute_dni()
# new_solar_db.save()

new_solar_db = Sarah2(h5_path=h5file)

# Compute descriptors
batches = Batches(oven, new_solar_db, h5_to)
# batches.run(nb_processes=24, chunk_size=10000)
# nb_batches = batches.compute_raster(method=np.mean, groupby="M",
#                                     resampling_rule="M", resampler="sum",
#                                     nb_processes=24, chunk_size=100)
# nb_batches.to_file(os.path.join(result_dir, "batches_months.tif"))

# Other scenario: week strategy
nb_batches = batches.compute_raster(method=np.mean, groupby="M",
                                    resampling_rule="W", resampler=week_strategy,
                                    nb_processes=24, chunk_size=100)
nb_batches.to_file(os.path.join(result_dir, "batches_months_week_strategy.tif"))


# Other scenario


# Population density
# pop = PopDensity(gpd.GeoDataFrame.from_file(puy_saint_andre))
# pop.create_mesh(grid_spacing=250)
# pop.from_raster(france_pop, output_field="POP_GRID").to_file(os.path.join(pop_out_dir,
#                                                                           "puy_saint_andre_250.geojson"),
#                                                              driver="GeoJSON")
