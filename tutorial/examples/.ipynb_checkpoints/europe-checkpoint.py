from pyrasta.raster import Raster
from scipy.stats.distributions import norm

from sbk.analysis import Variable, BreadAccessibility, BreadViability, \
    MEDIUM_R_DIST
from sbk.analysis.default_samples import MEDIUM_P_C_B, YEARLY_HIGH_X_P, YEARLY_VERY_HIGH_X_P, \
    YEARLY_MEDIUM_X_P, YEARLY_LOW_X_P, YEARLY_VERY_LOW_X_P, HIGH_R_DIST

bread_qty_per_batch = 38  # kg/batch

raster_batches = Raster("/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/INDICATORS/EUROPE/nb_batches"
                        ".tif")
raster_pop = Raster("/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/INDICATORS/EUROPE"
                    "/pop_europe_2020_1km.tif")

nb_batches = Variable(raster_batches, no_data=raster_batches.no_data)
pop = Variable(raster_pop, no_data=raster_pop.no_data)

bread_production = nb_batches * bread_qty_per_batch
d_nom = norm(loc=HIGH_R_DIST.mean(), scale=0.1)

b_a = BreadAccessibility(population_density=pop,
                         prob_of_being_a_b_consumer=MEDIUM_P_C_B,
                         avg_radius_of_access=d_nom)

b_a.compute(chunksize=1000, distribution_size=10000)

b_v = BreadViability(b_a.variable,
                     bread_production)

b_v.compute(distribution_size=10000)
b_a.to_out_format().to_file("/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/INDICATORS/EUROPE/bread_accessibility_2.tif")
b_v.to_out_format().to_file("/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/INDICATORS/EUROPE/bread_viability_2.tif")

# raster = b_a.to_out_format()
# raster.to_file("/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/INDICATORS/EUROPE/bread_accessibility.tif")
# prob_of_consumption = ProbabilityOfConsumption(bread_production,
#                                                pop,
#                                                bread_consumption)
#
# prob_of_consumption.compute(chunksize=1000, nb_processes=30)
# raster = prob_of_consumption.to_out_format()
# raster.to_file("/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/INDICATORS/EUROPE/prob_of_consumption"
#                ".tif")
