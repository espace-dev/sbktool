import os

from sbk.grid_db.sarah import Sarah3
from sbk.analysis.descriptors import Batches, Bakable

#############################
# Computing bk days
# * raster, netcdf file

in_dir = '/tmp/data/' + 'SWEDEN_JUNE_2020'
out_dir = "data"
nc_batches = os.path.join(out_dir, "nb_batches.nc")
nc_bk = os.path.join(out_dir, "bk_days.nc")
raster_bk = os.path.join(out_dir, "nb_bk_days.tif")

try:
    bk_days = Sarah3.build_from_dir(nc_bk, Bakable.name)
    bk_days = bk_days.resample("Y", "sum")
    raster = bk_days.to_raster()
    raster.to_file(raster_bk)

except FileNotFoundError:
    # Build raster for given descriptor (Batches) netcdf file
    batches = Sarah3.build_from_dir(nc_batches, Batches.name)
    bk_days = Bakable().compute(batches)
    bk_days.save(nc_bk)
