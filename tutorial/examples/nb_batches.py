import os

from sbk.grid_db.sarah import Sarah3
from sbk.analysis.descriptors import Batches
from sbk.bakery.oven import Oven
from sbk.bakery.models import SolarThresholdModel

#############################
# Computing Number of Batches
# * raster, netcdf file

in_dir = "/media/benjamin/storage/BOUSOLRUN/001_MAIN/CMSAF/FRANCE_2020"
out_dir = "/media/benjamin/storage/BOUSOLRUN/001_MAIN/RESULTS/EUROPE"
nc_file = os.path.join(out_dir, "nb_batches.nc")
raster_file = os.path.join(out_dir, "nb_batches_month.tif")

try:
    # Build raster for given descriptor (Batches) netcdf file
    print("Build raster (Batches) from netcdf file\n...")
    nb_batches = Sarah3.build_from_dir(nc_file, Batches.name)
    nb_batches = nb_batches.resample("M", "sum")
    raster = nb_batches.to_raster()
    raster.to_file(raster_file)

except FileNotFoundError:

    # Build netcdf file for given descriptor (Batches)
    print("Error\nBuild netcdf file instead ! Please wait ...")
    dni = Sarah3.build_from_dir(in_dir, "DNI")
    oven = Oven(SolarThresholdModel(700, 2, 1))
    batches = Batches(oven)
    batches.compute(dni)

    batches.resample(rule="D", resampler="sum")
    batches.save(nc_file)
