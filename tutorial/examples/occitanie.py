import geopandas as gpd
import os
import pandas as pd

from sbk.analysis import Batches, Variable, ProbabilityOfBConsumption, BreadTurnover
from sbk.analysis.default_samples import HIGH_P_C_B, LOW_R_DIST, MONTHLY_MEDIUM_Q_C, MEDIUM_R_DIST, \
    MONTHLY_HIGH_Q_C, HIGH_P_H_C, MEDIUM_P_C_B
from sbk.grid_db.sarah import Sarah3

in_dir = "/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/RESULTS/EUROPE"
out_dir = "/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/RESULTS/OCCITANIE"
nc_file = os.path.join(in_dir, "nb_batches.nc")
batch_file = os.path.join(out_dir, "occitanie_batches.csv")
pop_file = os.path.join(out_dir, "occitanie_pop.csv")
pc_file = os.path.join(out_dir, "occitanie_pc.csv")
bt_file = os.path.join(out_dir, "occitanie_bt.csv")

if not os.path.exists(batch_file):

    nb_batches = Sarah3.build_from_dir(nc_file, Batches.name)
    nb_batches = nb_batches.resample("M", "sum")

    poi_occitanie = gpd.GeoDataFrame.from_file("/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/"
                                               "TOURISM/selected_poi_occitanie_epsg_4326.geojson")

    poi_ts = nb_batches.extract_timeseries(poi_occitanie, "mean",
                                           region_names=poi_occitanie["tourism"].to_list())

    dni = pd.concat(poi_ts, axis=1)
    pop_density = pd.concat([pd.Series(poi_occitanie.loc[n, "pop_mean":"pop6_mean"].values,
                                       name=name) for n, name in
                             enumerate(poi_occitanie["tourism"].to_list())], axis=1)

    dni = dni.iloc[(dni.index.month >= 5) & (dni.index.month <= 10)]
    dni.to_csv(batch_file, sep=",")
    pop_density.to_csv(pop_file, sep=",")

else:
    bread_qty_per_batch = 38  # kg/batch
    dni = pd.read_csv(batch_file, index_col=0, parse_dates=True)
    pop_density = pd.read_csv(pop_file, index_col=0) * 100  # hab/km²

    bread_production = Variable(dni) * bread_qty_per_batch
    pop_d = Variable(pop_density)

    bread_turnover = BreadTurnover(bread_production,
                                   pop_d,
                                   MEDIUM_P_C_B,
                                   LOW_R_DIST,
                                   MONTHLY_HIGH_Q_C,
                                   HIGH_P_H_C).compute(0.3,
                                                       10000)

    bread_turnover.to_out_format().to_csv(bt_file, sep=",")
