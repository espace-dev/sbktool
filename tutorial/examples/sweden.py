import geopandas as gpd
import numpy as np
import pandas as pd
import os

from sbk.analysis import Batches, Variable, ProbabilityOfBConsumption, BreadTurnover
from sbk.analysis.default_samples import LOW_R_DIST, _truncnorm, MEDIUM_R_DIST
from sbk.bakery.models import SolarThresholdModel
from sbk.bakery.oven import Oven
from sbk.grid_db.sarah import Sarah3

#############################
# Computing Number of Batches
# * raster, netcdf file

sweden_cities = ("/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/GEODATA/SWEDEN/CITIES/main_cities"
                 ".geojson")
in_dir = "/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/CMSAF/SWEDEN_2020"
out_dir = "/media/benjamin/STORAGE/BOUSOLRUN/001_MAIN/RESULTS/SWEDEN"
nc_file = os.path.join(out_dir, "nb_batches.nc")
raster_file = os.path.join(out_dir, "nb_batches.tif")
csv_file_mean = os.path.join(out_dir, "sweden_batches.csv")
csv_file_max = os.path.join(out_dir, "sweden_batches_max.csv")
pop_csv_file = os.path.join(out_dir, "sweden_pop.csv")
bt_file = os.path.join(out_dir, "sweden_bt.csv")
sbp_file = os.path.join(out_dir, "sweden_sbp.csv")
er_file = os.path.join(out_dir, "sweden_er.csv")

try:

    economic_rate = 8  # €/kg
    bread_qty_per_batch = 38  # kg/batch
    dni = pd.read_csv(csv_file_max, index_col=0, parse_dates=True)
    pop_density = pd.read_csv(pop_csv_file, index_col=0)  # hab/km²

    # bread_production = 250  # kg/week
    solar_bread_production = Variable(dni) * bread_qty_per_batch
    pop_d = Variable(pop_density)
    full_bread_production = Variable.full(250, pop_d)

    swedish_consumption = _truncnorm(0.472, 0.26, [0.05, np.inf])
    swedish_whole_grain_cons = _truncnorm(526/1435, 50/1435, [0, 1])
    swedish_bread_consumer = _truncnorm(0.98, 0.02, [0, 1])
    b_t = BreadTurnover(full_bread_production,
                        pop_d,
                        swedish_whole_grain_cons,
                        LOW_R_DIST,
                        swedish_consumption,
                        swedish_bread_consumer).compute(2,
                                                        10000)
    economic_return = b_t.variable * economic_rate
    b_t.to_out_format().to_csv(bt_file, sep=",")
    economic_return.to_out_format().to_csv(er_file, sep=",")
    solar_bread_production.to_out_format().to_csv(sbp_file, sep=",")

except FileNotFoundError:

    try:

        nb_batches = Sarah3.build_from_dir(nc_file, Batches.name)
        if os.path.exists(raster_file):
            if not os.path.exists(csv_file_mean):
                stat = "sum"
                csv_file = csv_file_mean
            else:
                stat = "max"
                csv_file = csv_file_max
            print(f"Build '{stat}' csv file over main cities")
            region = gpd.GeoDataFrame.from_file(sweden_cities)
            nb_batches = nb_batches.resample("W", stat)
            sweden_ts = nb_batches.extract_timeseries(region, "mean",
                                                      region_names=region["name_en"].to_list())

            results = pd.concat(sweden_ts, axis=1)
            results.to_csv(csv_file, sep=",")

            pop_density = pd.DataFrame(index=results.index,
                                       columns=results.columns)

            for val, col in zip(region["pop_mean"], results.columns):
                pop_density[col] = val

            pop_density.to_csv(pop_csv_file)

        else:
            print("Build 'nb of Batches' raster file")
            # Build raster for given descriptor (Batches) netcdf file
            nb_batches = nb_batches.resample("Y", "sum")
            raster = nb_batches.to_raster()
            raster.to_file(raster_file)

    except FileNotFoundError:

        print("Build 'Batches' netcdf file")
        # Build netcdf file for given descriptor (Batches)
        dni = Sarah3.build_from_dir(in_dir, "DNI")
        oven = Oven(SolarThresholdModel(700, 2, 1))
        batches = Batches(oven)
        batches.compute(dni)

        batches.resample(rule="D", resampler="sum")
        batches.save(nc_file)
