import numpy as np

from pyrasta.raster import Raster
from scipy.stats.distributions import norm

from sbk.analysis import Variable, BreadAccessibility, BreadViability
from sbk.analysis.default_samples import HIGH_R_DIST, HIGH_P_C_B, _truncnorm, LOW_R_DIST

bread_qty_per_batch = 38  # kg/batch

raster_batches = Raster("/media/benjamin/storage/BOUSOLRUN/001_MAIN/INDICATORS/SWEDEN/nb_batches"
                        ".tif")
raster_pop = Raster("/media/benjamin/storage/BOUSOLRUN/001_MAIN/INDICATORS/SWEDEN/pop_1km.tif")

nom_bread_prod = _truncnorm(6000, 1000, [4000, 8000])
swedish_consumption = _truncnorm(24.5, 13.5, [0, np.inf])
swedish_whole_grain_cons = _truncnorm(526/1435, 50/1435, [0, 1])
swedish_bread_consumer = _truncnorm(0.98, 0.02, [0, 1])

nb_batches = Variable(raster_batches, no_data=raster_batches.no_data)
pop = Variable(raster_pop, no_data=raster_pop.no_data)

bread_production = nb_batches * bread_qty_per_batch
d_nom = norm(loc=LOW_R_DIST.mean(), scale=0.025)

b_a = BreadAccessibility(population_density=pop,
                         nominal_bread_production=nom_bread_prod,
                         bread_consumption=swedish_consumption,
                         prob_of_being_a_consumer=swedish_bread_consumer,
                         prob_of_being_a_b_consumer=swedish_whole_grain_cons,
                         avg_radius_of_access=d_nom)

b_a.compute(chunksize=1000, distribution_size=10000)

b_v = BreadViability(b_a.variable,
                     bread_production,
                     nominal_bread_production=nom_bread_prod)

b_v.compute(distribution_size=10000)
raster = b_v.to_out_format()
raster.to_file("/media/benjamin/storage/BOUSOLRUN/001_MAIN/INDICATORS/SWEDEN/bread_viability.tif")
